# libzedr

_libzedr_ is a library which helps you make zero down-time restartable stateless services on Linux. It works by preserving file descriptors across a restart by sending it over Unix socket to a restart server. Restart server can be provided by a service manager such as systemd or the service can be restarted without any external support when it does not drop privileges.

## I am a service developer. How can I use libzedr?

Your service can look like this:

```c
#include <libzedr.h>

int init_fd(void* arg);

int main(int argc, char** argv) {
    zedr_init(argc, argv);
    /* ... */
    int fd = zedr_fd_init(init_fd, NULL);
    /* ... */
    while (true) {
        int nfd = zedr_accept(fd, &addr, sizeof(addr));
        /* ... */
    }
}
```

...and to restart, send the process `SIGHUP` signal. That's all!

You can learn more in [docs/DEVELOPER.md](docs/DEVELOPER.md).


## I am an administrator. How can I manage services using the library?

In order to update the service, replace the executable and send the service the restart signal, which is by default SIGHUP. That's all!

The services can currently run in two modes:

* standalone - see [docs/STANDALONE_USAGE.md](docs/STANDALONE_USAGE.md)
* systemd - see [docs/SYSTEMD_USAGE.md](docs/SYSTEMD_USAGE.md)

## I am a service manager developer. How can I support libzedr in my service manager?

First, read the guide for administrators and service developers. After that, all you need is the `include/libzedr-sm.h` header file. How to implement a service manager integration is written in the documentation comments there.

## I am a developer. How can I hack on libzedr?

See [docs/LIBDEVEL.md](docs/LIBDEVEL.md)

---

## Doxygen-generated version of this document

[CI-built publicly accesible version can be found here.](https://gitlab.com/vakabus/libzedr/-/jobs/artifacts/master/file/docs/html/index.html?job=docs)

Detailed user documentation can be build using Doxygen by calling `doxygen` command in project's top-level directory without any arguments. The generated documentation can be viewed by opening a `docs/html/index.html` file in a web browser. You can also download prebuilt documentation for the current `master` branch [here](https://gitlab.com/vakabus/libzedr/-/jobs/artifacts/master/download?job=user_docs).


