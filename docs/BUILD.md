# Build libzedr

[Meson build system](https://mesonbuild.com/) is used to manage builds. See their documentation for installation instructions.

Run this to build the project. It's not necessary to run the first command more than once.

```sh
meson build_dir
ninja -C build_dir
```

The `libzedr.so` is then located in the `build_dir` directory. Header files are in the top-level `include` directory. You can install the library into your system by running `ninja -C build_dir install`. This also installs configuration files for `pkg-config`. For details, see [the Meson documentation](https://mesonbuild.com/Running-Meson.html#installing).

## Test

```sh
meson build_dir
ninja -C build_dir
python test/runner.py
ninja -C build_dir test
```

## Dependencies

* pthreads
