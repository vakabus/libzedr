# Using libzedr in a service

Libzedr provides transparent restarts for services using the library. However, the service has to be stateless or must already handle state serialization.

## What exactly does the library do?

Even stateless services have state such as the current working directory, environment variables and such. And most importantly, the file descriptors used for receiving requests. Libzedr tries to safe this state and pass it onto the new restarted instance in order to provide transparent restarts.

Your service receives a restart signal. That is handled by the library, which sets a restart flag. Because your service must contain restart points (safe points for restart), a restart point must be then reached. This starts the restart procedure, which passes the state onto a new instance. After that, the service must gracefully terminate. If you put restart points in every non-terminating thread, libzedr can provide this functionality for you.

## Usage

The service you want to restart has to follow these steps to use libzedr:

1. optionally configure the library with `zedr_config_` functions
2. initialize the library with `zedr_init()`
3. create the file descriptor by `zedr_fd_init()`
4. handle requests and make sure to have `zedr_restart_point()` in every thread (or if not in every, then configure a custom graceful shutdown)

All these function can be included by [the `libzedr.h` file](../include/libzedr.h). The header file also contains their documentation (best viewed by Doxygen).

## Building a project with libzedr

If you want to build the library itself, see [BUILD.md](BUILD.md).

Use the `-lzedr` flag to link with libzedr. If you use Meson as your build system, you can also use libzedr as a subproject (our [modification of systemd does that](https://gitlab.com/vakabus/libzedr_systemd/-/blob/libzedr/meson.build#L1376))
