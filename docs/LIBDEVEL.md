# Modifying the libzedr library

## Structure

* `src/`
  * `utils.[ch]` contain generic system utilities like FD manipulation and signalling
  * `fd_exchange_protocol.[ch]` implementation of FD passing protocol over a given Unix socket
  * `restart_server.[ch]` implements the restart server - saves and restores state, manages the Unix socket, the API for service managers
  * `libzedr.c` contains user facing logic of the library, the API for services
* `include/` directory contains publicly exposed interface for the library
  * `libzedr.h` - implemented by `src/libzedr.c`
  * `libzedr-sm.h` - implemented by `src/restart_server.c`
* `test/`
  * `runner.py` manages high-level integration tests
  * unit tests are manager by Meson (i.e. `ninja test`)

## Integration tests

Tests with actual services. We test with a dummy service serving primes. Every test has two parts - the server and the client. The server is the same server as one would use in production. It just accepts requests and responds to them. The client send requests in a loop and signals its parent process with `SIGUSR1` and `SIGUSR2` after every successful, resp. failed request.

You can find the list of tests at the end of the `runner.py` script.
