# Managing restarts in standalone mode

To transparently restart a service, send it the restart signal (by default SIGHUP). That's really all there is!

## Limitations of standalone mode

If the service restarts in standalone mode, it can't elevate it privileges. Services in production should drop privilege after starting, so this mode is not well suited for that use case. However, the way libzedr provides transparent restart means, that you can use just the same binary under systemd and it will work automatically.

## How does it work?

When the service receives a signal, it forks a new process -- the restart server. The restart server receives some state from its parent by a Unix socket, some is inherited through the fork. It reconfigures itself to mirror startup conditions of the original process and execs itself again. During that, the old service instance is left to gracefully terminate.
