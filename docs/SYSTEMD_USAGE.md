# Managing restarts under systemd

NOTE: libzedr currently requires modified version of systemd. You can find its source [here](https://gitlab.com/vakabus/libzedr_systemd).

If you already have a transparently restartable service configured on your system and you want to restart it, you don't have to do anything special. Just call the usual `systemctl restart` command. If, for some reason, you want to restart it normally, stop the service by `systemctl stop` and start it again by `systemctl start`.

## Example service file

The following configuration is all you need in the majority of cases.

```
[Unit]
Description=Libzedr capable server

[Service]
ExecStart=/usr/bin/server
RestartTransparently=true
```

## Service file options

Our modified version of systemd adds these directives to the `[Service]` section of the unit:

```
RestartTransparently={yes,no,true,false}
RestartTransparentTimeoutSec={number of seconds that a restart can take}
RestartTransparentSignal={signal name or signal number}
RestartTransparentSocketDir={a directory}
```

The `RestartTransparently` directive enables transparent restarts. All other directives have no effect if this is false. If the service developer didn't provide any more details, this is the only directive required to enable the functionality. Everything else should work out of the box. Disabled by default.

The `RestartTransparentTimeoutSec` directive specifies how long will systemd wait until it receives file descriptors from the service. After this time, the service will get restarted normally. The reason is, that the service receives a signal to initiate the restart. If that signal is handled incorrectly (which should not happen), the service might end up in an anomalous state. It's then safer to restart the service forcefully and do it quickly, then leave it running. Default value is the global systemd's default timeout for service stop, usually a couple of seconds.

The `RestartTransparentSignal` directive reconfigures the signal that is used to notify the service about a transparent restart. This value must match with a value configured in the service's code. If the developer did not specify, which signal is used, you do not have to configure anything. Default value is SIGHUP.

The `RestartTransparentSocketDir` directive reconfigures the directory for management Unix socket during restart. Libzedr validates the PID of communicating process, so that it can be located in a publicly accessible directory. The configuration must match the configuration within the service's code. Default value is `/tmp`.

## Socket activation

Because libzedr uses internally the same protocol to pass file descriptors into the service, socket activation can be used for free with any service using libzedr.

Internally, libzedr matches file descriptors to their corresponding use by an 64bit ID. This ID is specified in the source code of the service or it is by default ZEDR_DEFAULT_FD_ID (i.e. value 0). If the service uses more than one file descriptor to listen on, you must know the IDs to continue. If the service uses only one, use the default ID 0.

For every listening file decriptor, create a separate socket unit. Systemd allows multiple file descriptors per socket unit, but they all share the same name.

```
[Unit] 
Description = libzedr socket-activated example

[Socket]
Service=server.service
ListenStream = 4444 
FileDescriptorName=0

[Install] 
WantedBy = sockets.target
```

The `ListenStream` directive can be replaced by any other file descriptor defining directive. The file descriptor ID must be written in the `FileDescriptorName` directive. The `Accept` directive must be set to `no`, but that is the default so it does not have to be explicitly specified.

## How does it work?

First, have a look at how do standalone restarts work in [STANDALONE_USAGE.md](STANDALONE_USAGE.md).

The restart procedure is almost the same as in the standalone mode. We said that the process spawns a restart server after it receives a signal. That's not the full picture, because first it checks for an already existing restart server. A new restart server is spawned only if it does not exist.

Libzedr provides the same restart server implementation for service managers. So, when you invoke `systemctl restart service_name`, systemd first starts a restart server and then it sends the restart signal to the process. The process sends its file descriptors to systemd without starting its own restart server and systemd uses the file descriptors to start a new instance.

An important note is, that a restart server is bound to a specific PID. For every service, there is a restart server (not necessarly a process, but logically). It is therefore safe to use some services under systemd and some in standalone mode. Nothing is shared between the processes.
