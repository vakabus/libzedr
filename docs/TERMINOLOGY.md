# Terminology

It's necessary to introduce some short names for some concepts to allow for easier discussion about them. This document contains a list of such definitions.

* _accept loop_
  * A loop in a service which accepts new requests by calling `accept` on the server socket. Could be used in conjuction with `poll` or `epoll`.
* _restart point_
  * A place in code where it is safe to restart the service when reached. That means, that the current execution thread can be stopped without interrupting any request handlers.
