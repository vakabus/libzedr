# Random notes

This file is a scratchpad for any thoughts during development. Do not expect anything serious. Notes could be in any language.

----

* ssh-agent nebo gpg-agent jako example program ???
* TODO rozvrh

---

* rozsirit api na korektni ukoncovani
* async io - experimentovat, ale tezko rict
* a co s knihovnama?
* userspace networking stack? asi nas nezajima


* testy
  * paralelni - vsichni volaji accept nebo jen nekdo distribuuje praci
  * testy
    - projde jeden restart?
    - stress testy s nekolikanasobnym restartem

* metriky na pozdejsi analyzu
    * jak moc se snizi behem restartu throughput
    * jak moc se zmeni latence pri restartu

- what if init_function supplies FD with O_CLOEXEC?
  * nereseni

* socket activated start resi, jak ziskavam socket pri startu, kouknout se na to
* otestovat, zda uprava argumentu meni /proc/self/cmdline - zakazat v modelu


----


~~packed struct na posilani dat?~~
~~ulozit si na zacatku nekam bokem FD 0,1,2, a CWD, env. Pak restore~~


----

buildeni primo ve virtualce nebo v kontejneru
~~pojmenovani UseLibzedr spis na neco jako RestartTransparently~~
systemctl edit user@1003.service a upravit cestu na binarku, ExecStart=se musi nejak prepsat, takze prvni musi byt prazdny

----

~~bugfix~~
pidfd_getfd
~~testy v kontejneru~~
socket object? neni to mozny pristup?
mereni?
~~domluvit se s Tumou, ukazat mu to a jak to delat z akademickeho pohledu~~
~~zacit psat~~

---

~~kdyz bezi uz nekde server a pokusim se v systemd spustit novy, tak by nemel bezet zero downtime restart~~
kdyz jsem ve stavu transparent_restart_pending, tak se neda prihlasit


29 kveten - odevzdavame vse
~~konfigurovatelny SIGNAL~~
~~poslat mail s textem Tumovi a Michalovi~~

---

když zůstane stara instance a pak se zavola systemctl stop, ukonci se to?

~~libzedr-systemd.h - zmenit naming~~
~~nagativní testy~~
test změny umístění socketu

~~optional závislost na libsystemd~~
~~predavani filedeskriptoru stejne jako sd_pid_notify_with_fds()~~

text - generational tracking
text - kdyby se to implementovalo ciste do systemd, jak by se to dalo udelat jinak; dalsi moznosti integrace do upstreamu
graceful shutdown od knihovny - best effort - nejde to udelat asi spravne, je potreba spolehnout na sluzbu
pri restartu bezi paralelne i stary process - musi se ukoncit i ta stara instance, aby nezpracovala pozadavky do nekonecna
zabití lame duck procesů po nějakém timeoutu?
* vanilla systemd - korektni ukonceni pak nefunguje
* benchmark?
* jak to funguje se systemd - admin doc
* zmínit upravu existujícího sw - rmbt-server
* rozebrat moznosti budouciho rozvoje

~~konfigurovatelny ZEDR_SOCKET_BASE~~
~~bezpecnostni dira pri predavani FD? Rogue restart server~~
~~omezeni prav FS socketu - kontrola pidu odesilatele~~

!!! zohledneni socketove aktivace a deaktivace - aspon zanalyzovat
socketova aktivace - predavani IDcek neni definovane, protoze IDcka nefunguji
~~* drzi si kopii na ten socket celou dobu?~~ ANO, drzi


* zkusit integraci s jinym service manager (init skript, supervisor)
~~* zkusit gpg-agent nebo webove servery~~

* jako ukazku rmbt server
* stateless-service - co s requestem, ktery je ve vice zpravach? Jak to nadefinovat tak, aby tohle bylo zakazane.


---

~~1. reference~~
~~2. opravit systemd~~
~~3. evaluace ~~
~~4. poslat Tůmovi (Michal v cc)~~
~~5. zjistit, jak s oddělením pro Michala~~
~~6. zjistit, jak s přílohama (do SISu, je povinná sekce ~~~~attachements? jak to tam má být)~~
~~7. libzedr - víc FD~~
~~8. dokumentaci do Doxygenu~~
9. RMBT server

~~* department u Michala - SUSE?~~
~~* badness~~
~~* pointery u FDs~~
* reference na issue
~~* nemuzou být v listech v cgroup v2~~
~~* zmínit v evaluaci, že se to dá řešit i jinak~~
~~* git commit~~

---
  
* dodělat a odevzdat
* testy s AF_UNIX
* multiple bind??? Co to dělá?
~~* odkaz v README na ten header file~~
~~* číslování řádků u glibc~~
~~* odkaz přes číslo sekce, ne přes text~~

~~* code snippety - netreba dělat~~

~~* simpledia - atomic => nebudu to dělat, protože to reálně atomické není a navíc to nevadí~~
~~* sleeping barber - už přesně nevím, co tahle poznámka měla znamenat~~
~~* complexdia - signal handler...~~
* graceful shutdown - evaluation - It would work well enough for services not spawning threads often.
