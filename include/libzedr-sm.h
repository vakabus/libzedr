/**
 * @file libzedr-sm.h
 * 
 * # Libzedr service manager integration
 * 
 * There are two parts to integrating support into a service manager. First,
 * the service manager must be able to retrieve FDs from the service itself.
 * That is what this library provides. The second part is starting the service
 * while passing on the FDs. That part is not implemented here, because it is
 * highly dependent on the concrete implementation of the service manager.
 * 
 * ## Stopping running service and extracting FDs
 * 
 * To stop a service that is using libzedr, you can call zedr_sm_server_run().
 * This call will start a restart server, which means that it will create
 * a Unix socket to send the FDs over. Then, it will signal the process that
 * it should restart and it will wait for FDs to be received (or timeout).
 * After that the restart server cleans up after itself and the function
 * returns the retrieved FDs.
 * 
 * This process takes some time and if the design of the service manager uses
 * asynchronous event loop, it is possible to achieve the same by using
 * zedr_sm_server_init() to create the Unix socket and signal the service's
 * process that it should restart. Then, when an activity occurs on the socket,
 * zedr_sm_server_process() can be called to handle it.
 * 
 * ### The FD exchange protocol
 * 
 * The service manager does not have to care about restart server
 * implementation details. The protocol itself is internal and it is not
 * expected, that others will reimplement it.
 * 
 * ## Starting a new service instance
 * 
 * The way FDs are passed into a new service was designed after systemd's
 * socket activated services. The mechanism is the same.
 * 
 * The first FD passed in has a value of 3, second 4 and further on. The number
 * of passed in file descriptors is stored within environment variable named
 * `LISTEN_FDS`. Variable `LISTEN_NAMES` contain a comma-separated list of IDs
 * as a string and `LISTEN_PID` variable must contain the PID of the new
 * service's process.
 * 
 * ----
 * 
 * Note: Methods defined here are implemented in file src/restart_server.c.
 */

#ifndef _LIBZEDR_SYSTEMD_H
#define _LIBZEDR_SYSTEMD_H

#include <unistd.h>
#include <stdint.h>

/**
 * Opens a control socket for communication with the restarted service. Can be used in async
 * service managers calling select()/poll() on FDs. When some activity appears,
 * zedr_sm_server_process() should be called.
 * 
 * @param process_pid Main PID of the service
 * @param restart_signum Signal that should be used to restart the service or 0 to use the default
 * @param server_socket_basedir Base directory that should contain the communication Unix socket. Use NULL for default.
 * @return FD of a restart server control socket. -1 when failed to create the socket. -2 when failed to kill() the process.
 *         Errno is set appropriately.
 * @sa zedr_sm_server_process()
 **/
int zedr_sm_server_init(pid_t process_pid, int restart_signum, char* server_socket_basedir);


/**
 * Reads data from restart server control socket, returns FDs that were passed
 * from the restarted application.
 * 
 * @param control_fd FD created by zedr_sm_server_init()
 * @param process_pid Main PID of the service. Must be the same as in zedr_sm_server_init()
 * @param server_socket_basedir Base directory that should contain the communication Unix socket. Use NULL for default.
 * @param timeout_ms How long should we wait for data (0 means no waiting)
 * @param fds Pointer to an array, that will be allocated as a result and it will contain retrieved FDs. Must be freed.
 * @param ids Pointer to an array, that will be allocated as a result and it will contain IDs of the FDs. Must be freed.
 * @return Lenght of the allocated arrays. Negative in case of an error.
 **/
int zedr_sm_server_process(int control_fd, pid_t process_pid, char* server_socket_basedir, int timeout_ms, int *fds[], uint64_t *ids[]);

/**
 * Cleans up after zedr_sm_server_init() when the restart procedure is aborted. It is safe to call this function after
 * zedr_sm_server_process() or zedr_sm_server_run(), but it is not required. Those functions clean up after themselves.
 *
 * @param control_fd FD created by zedr_sm_server_init()
 * @param process_pid Main PID of the service. Must be the same as in zedr_sm_server_init()
 * @param server_socket_basedir Base directory that should contain the communication Unix socket. Use NULL for default.
 **/
void zedr_sm_server_cleanup(int control_fd, pid_t process_pid, char* server_socket_basedir);


/**
 * Opens socket for communication with the restarted service. Wait for data to
 * appear on it and return number of returned fds. Negative return value is an error.
 * 
 * This function is just a wrapper around zedr_sm_server_init() and 
 * zedr_sm_server_process(), called one after the other.
 * 
 * @param process_pid Main PID of the service.
 * @param restart_signum Signal that should be used to restart the service or 0 to use the default
 * @param server_socket_basedir Base directory that should contain the communication Unix socket. Use NULL for default.
 * @param timeout_ms How long should we wait for data (0 means no waiting)
 * @param fds Pointer to an array, that will be allocated as a result and it will contain retrieved FDs. Must be freed.
 * @param ids Pointer to an array, that will be allocated as a result and it will contain IDs of the FDs. Must be freed.
 * @return Lenght of the allocated arrays. Negative in case of an error.
 * @sa zedr_sm_server_init() and zedr_sm_server_process()
 **/
int zedr_sm_server_run(pid_t process_pid, int restart_signum, char* server_socket_basedir, int timeout_ms, int *fds[], uint64_t *ids[]);

#endif
