#ifndef _LIBZEDR_H
#define _LIBZEDR_H

#include <sys/types.h>
#include <sys/socket.h>
#include <poll.h>
#include <signal.h>
#include <stdint.h>

/**
 * A name of an environment variable, that when set forces libzedr
 * to pass FDs to systemd using libsystemd.
 **/
#define ZEDR_USE_LIBSYSTEMD_ENV_VAR "LIBZEDR_USE_LIBSYSTEMD"

/**
 * A directory that contains Unix sockets used by libzedr for sending FDs
 **/
#define ZEDR_DEFAULT_SOCKET_BASEDIR "/tmp"

/**
 * Printf-compatible pattern restart server communication Unix socket bind name.
 **/
#define ZEDR_SOCKET_NAME_PATTERN "/libzedr_%d.socket"

/**
 * Default signal used for scheduling an restart.
 **/
#define ZEDR_DEFAULT_SIGNAL SIGHUP


/**
 * FD's ID used by the zedr_fd_init() function
 **/
#define ZEDR_DEFAULT_FD_ID 0

/**
 * Function pointer type used by zedr_fd_init().
 * 
 * Function which takes an arbitrary argument and returns a file descriptor. It will get called only
 * when there is no file descriptor passed in from previous instances of the service.
 * 
 **/
typedef int (*zedr_init_socket_f)(void*);

/**
 * Function pointer type used by zedr_config_enable_custom_graceful_shutdown()
 * 
 * Function that gets called when restart is scheduled. It should stop the whole service gracefully.
 * It is called by the first restart point that is executed after the restart signal. The function
 * is the last thing the restart point calls. If it returns, the calling thread will continue through
 * the restart point just as it would after any function.
 **/
typedef void (*zedr_graceful_shutdown_f)(void*);

/**
 * Libzedr configuration. Disables library provided graceful shutdown
 * through restart points in every thread and uses existing service
 * method instead.
 * 
 * @invariant Must be called before first restart point.
 * @param func Function that will be used by libzedr to shutdown the whole service
 * @param arg Arbitrary argument for zedr_graceful_shutdown_f
 * @sa zedr_restart_point()
 **/
void zedr_config_enable_custom_graceful_shutdown(const zedr_graceful_shutdown_f func, void *arg);

/**
 * Libzedr configuration. Changes the signal used to signal restarts. By default,
 * it is ZEDR_DEFAULT_SIGNAL.
 * 
 * @invariant Must be called before zedr_init() function.
 * @param signum Signal number that should be used for scheduling restarts.
 **/
void zedr_config_set_restart_signal(int signum);

/**
 * Libzedr configuration. Changes the base directory used for Unix sockets during restart.
 * By default, it is ZEDR_DEFAULT_SOCKET_BASEDIR.
 * 
 * Note: Restart server validates which PID is sending over FDs. There is no need to
 * put the socket into an inaccessible directory to prevent that.
 * 
 * @invariant Must be called before zedr_init() function.
 * @param dirnam Null-terminated string with a directory that will contain the Unix socket. The settings
 *               is not validated.
 **/
void zedr_config_set_restart_socket_dir(char* dirname);

/**
 * This function initializes the library. It must be called as soon as possible after main() start,
 * before any other initialization takes place. Most importantly before any network initialisation
 * and before zedr_fd_init().
 * 
 * @invariant Must be called before zedr_fd_init() and any zedr_restart_point()
 * @param argc Argument count passed directly from main's arguments
 * @param argv Argument vector passed directly from main's arguments
 **/
void zedr_init(int argc, char** argv);

/**
 * Initializes the server FD only when it's required. When running after restart,
 * the initialization function is not called and FD retrieved from previous service instance
 * is returned.
 * 
 * @invariant Must be called after zedr_init()
 * @param func FD initialization function
 * @param arg parameter passed to the initialization function
 * @return Server FD used for listening
 **/
int zedr_fd_init(const zedr_init_socket_f func, void* arg);

/**
 * Initializes the server FD only when it's required. When running after restart,
 * the initialization function is not called and FD retrieved from previous service instance
 * is returned.
 * 
 * @invariant Must be called after zedr_init()
 * @param func FD initialization function
 * @param arg parameter passed to the initialization function
 * @param id An identifier for the file descriptors when using multiple file descriptors at once
 * @return Server FD used for listening
 **/
int zedr_fd_init_with_id(const zedr_init_socket_f func, void* arg, uint64_t id);

/**
 * Marker for a place in code, where a restart can occur. Must be present in every non-terminating
 * thread or graceful shutdown must be manually configured.
 * 
 * The method tests whether a restart is scheduled. The first thread that hits restart point after
 * restart is signalled, starts the restart procedure. The first thing that is done is that
 * FDs are passed to a restart server. Restart server is either external (e.g. systemd) or internal
 * (automatically created by a fork). After that, there are two possible scenarios:
 * 
 * @li When a graceful shutdown is configured, the restart point calls the gracefull shutdown function.
 * Any other thread that runs into restart_point does nothing.
 * 
 * @li When no explicit configuration is made, libzedr tries to implement graceful shutdown by itself.
 * In this case a restart point must be present in every non-terminating thread, because every other thread
 * that runs into a restart point immediately terminates. Some threads might be stuck in a syscall and
 * to help those, the first thread hitting restart point sends signal to every thread one by one in order to
 * interrupt those syscalls.
 * 
 * Libzedr provides some wrappers around usual blocking syscalls such as accept() or poll(). The only thing
 * those wrappers do is call a restart point, then syscall and if an EINTR error occurs, call restart point
 * again. This guarantees, that a blocking syscall will be properly interrupted by a signal.
 * 
 **/
void zedr_restart_point(void);

/**
 * Restart-point wrapper around accept() syscall. See zedr_restart_point() for more details. Semantics are the
 * same as for ordinary syscall.
 * 
 * @sa zedr_restart_point()
 **/
int zedr_accept(int socket, struct sockaddr *addr, socklen_t *address_len);

/**
 * Restart-point wrapper around poll() syscall. See zedr_restart_point() for more details. Semantics are the
 * same as for ordinary syscall.
 * 
 * @sa zedr_restart_point()
 **/
int zedr_poll(struct pollfd fds[], nfds_t nfds, int timeout);

/**
 * Restart-point wrapper around read() syscall. See zedr_restart_point() for more details. Semantics are the
 * same as for ordinary syscall.
 * 
 * @sa zedr_restart_point()
 **/
ssize_t zedr_read(int fd, void* buf, size_t count);

/**
 * Restart-point wrapper around recv() syscall. See zedr_restart_point() for more details. Semantics are the
 * same as for ordinary syscall.
 * 
 * @sa zedr_restart_point()
 **/
ssize_t zedr_recv(int sockfd, void *buf, size_t len, int flags);

#endif
