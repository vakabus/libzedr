#define _GNU_SOURCE

#include <stdlib.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <time.h>
#include <errno.h>
#include <string.h>
#include <poll.h>
#include <fcntl.h>
#include <unistd.h>

#include "fd_exchange_protocol.h"
#include "utils.h"

#define ARGUMENTS_BUFF_SIZE 4096
#define FDS_MAX_COUNT 8
#define PROTOCOL_MAGIC (0x1001F00F + ARGUMENTS_BUFF_SIZE * FDS_MAX_COUNT)

typedef struct fd_ex {
    uint64_t id;
    int fd;
    struct fd_ex *prev;
} fd_ex_t;

struct fd_exchange_msg {
    fd_ex_t *fd_last;
    int fd_count;
    char* arguments;
    int arguments_len;
};

void fd_exchange_msg_destroy(fd_exchange_msg_t *msg) {
    if (msg == NULL) return;

    /* pop all fds */
    while (fd_exchange_msg_pop(msg, NULL) != -1) {};
    
    free(msg->arguments);
    assert(msg->fd_count == 0);
    assert(msg->fd_last == NULL);

    free(msg);
}

fd_exchange_msg_t* fd_exchange_msg_create(int argc, char** argv) {
    /* allocate */
    fd_exchange_msg_t *msg = malloc(sizeof(*msg));
    if (msg == NULL) return NULL;

    /* fill in few defaults */
    msg->fd_last = NULL;
    msg->fd_count = 0;

    /* count total arguments length */
    uintptr_t arg_len = 1; // last null byte
    char** a = argv;
    while (*a != NULL) {
        arg_len += strlen(*a);
        arg_len += 1; // null byte
        a++;
    }
    
    /* allocate memory for arguments */
    msg->arguments = malloc(sizeof(char)*arg_len);
    msg->arguments_len = arg_len;
    if (msg->arguments == NULL) {
        free(msg);
        return NULL;
    }

    /* write arguments to the newly allocated place */
    a = argv;
    uintptr_t written = 0;
    while (*a != NULL) {
        strcpy(msg->arguments + written, *a);
        written += strlen(*a);
        a++;

        msg->arguments[written] = '\0';
        written++;

        assert(written < arg_len);
    }
    msg->arguments[written] = '\0';

    return msg;
}

static fd_exchange_msg_t* fd_exchange_msg_init_one_dimensional(char* arguments, int arguments_len) {
    fd_exchange_msg_t *msg = malloc(sizeof(*msg));
    if (msg == NULL) return NULL;

    msg->fd_last = NULL;
    msg->fd_count = 0;
    msg->arguments = malloc(sizeof(char)*arguments_len);
    memcpy(msg->arguments, arguments, arguments_len);
    msg->arguments_len = arguments_len;

    return msg;
}

int fd_exchange_msg_get_argv(fd_exchange_msg_t *msg, char*** argv) {
    /* count actual arguments present in the msg */
    int arg_count = 0;
    for (int i = 0; i < msg->arguments_len - 1; i++) {
        if (msg->arguments[i] == '\0') arg_count++;
    }

    /* create two dimensional array of arguments ending with NULL */
    char** a = calloc(arg_count+1, sizeof(char*));
    if (a == NULL) return -1;

    /* fill it with pointers to read data */
    int argc = 0;
    bool last_was_null = true;
    for (int i = 0; i < msg->arguments_len - 1; i++) {
        if (last_was_null) {
            a[argc] = msg->arguments+i;
            argc++;
            last_was_null = false;
        }
        if (msg->arguments[i] == '\0') last_was_null = true;
    }

    *argv = a;
    return argc;
}

int fd_exchange_msg_add(fd_exchange_msg_t *msg, uint64_t id, int fd) {
    fd_ex_t *f = malloc(sizeof(fd_ex_t));
    if (f == NULL) return -1;

    f->fd = fd;
    f->id = id;
    f->prev = msg->fd_last;

    msg->fd_last = f;
    msg->fd_count++;

    return 0;
}

int fd_exchange_msg_pop(fd_exchange_msg_t *msg, uint64_t *id) {
    if (msg->fd_count == 0) {
        assert(msg->fd_last == NULL);
        return -1;
    }

    fd_ex_t *f = msg->fd_last;
    msg->fd_last = f->prev;
    msg->fd_count--;

    if (id != NULL)
        *id = f->id;
    int fd = f->fd;
    free(f);
    return fd;
}

struct __attribute__((__packed__)) msg_data {
    uint32_t magic;
    uint32_t fd_count;
    uint64_t fd_ids[FDS_MAX_COUNT];
    uint32_t arguments_len;
    char arguments[ARGUMENTS_BUFF_SIZE];
};

int fd_exchange_send(int unixsocketfd, fd_exchange_msg_t *to_send) {
    struct msghdr msg;

    /* useless headers because we used connect() and already connected socket FD */
    msg.msg_name = NULL;
    msg.msg_namelen = 0;
    msg.msg_control = NULL;
    msg.msg_controllen = 0;

    struct msg_data data;
    memset(&data, 0, sizeof(data));
    data.magic = PROTOCOL_MAGIC;

    /* add FDs to the message if there are some */
    if (fd_exchange_msg_get_fd_count(to_send) > 0) {
        /* add control message to the actual message */
        int original_fdcount = to_send->fd_count;
        assert(FDS_MAX_COUNT > to_send->fd_count);
        char cmsg[CMSG_SPACE(sizeof(int)*FDS_MAX_COUNT)];  // control message for holding integers (actually a cmsghdr structure)
        memset(cmsg, 0, sizeof(cmsg));
        msg.msg_control = cmsg;
        msg.msg_controllen = CMSG_SPACE(sizeof(int)*original_fdcount);

        /* prepare content to send */
        int fds[FDS_MAX_COUNT];
        uint64_t id;
        for (int i = 0; fd_exchange_msg_get_fd_count(to_send) > 0; i++) {
            fds[i] = fd_exchange_msg_pop(to_send, &id);
            assert(fds[i] != -1);
            data.fd_count++;
            data.fd_ids[i] = id;
        }

        /* add FDs into ancilliary data */
        struct cmsghdr *cmsgp = CMSG_FIRSTHDR(&msg);
        cmsgp->cmsg_len = CMSG_LEN(sizeof(int)*original_fdcount);
        cmsgp->cmsg_level = SOL_SOCKET;
        cmsgp->cmsg_type = SCM_RIGHTS;
        memcpy(CMSG_DATA(cmsgp), fds, sizeof(int)*original_fdcount);
    }


    if (to_send->arguments_len > ARGUMENTS_BUFF_SIZE) return -1;
    memcpy(data.arguments, to_send->arguments, to_send->arguments_len);
    data.arguments_len = to_send->arguments_len;
    
    /* fill the message with data */
    struct iovec iov = {
        .iov_base = &data,
        .iov_len = sizeof(data)
    };
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;

    /* send */
    int size = sendmsg(unixsocketfd, &msg, 0);
    if (size == -1) {
        fprintf(stderr, "libzedr error: failed to send fd: %s\n", strerror(errno));
        return -1;
    }

    return 0;
}

int fd_exchange_recv(int unixsocketfd, fd_exchange_msg_t **received, pid_t service_pid, uint32_t timeout_ms) {
    /* wait for data to become available */
    struct pollfd pollfd = {
        .fd = unixsocketfd,
        .events = POLLIN,
        .revents = 0
    };
    int res = poll(&pollfd, 1, timeout_ms);
    if (res == 0) {
        fprintf(stderr, "libzedr restart server: timeout while receiving FDs\n");
        return -1;
    } else if (res < 0) {
        fprintf(stderr, "libzedr restart server: waiting for data using poll() failed: %s\n", strerror(errno));
        return -1;
    }


    struct msghdr msg;
    msg.msg_name = NULL;
    msg.msg_namelen = 0;

    /* prepare a place to read the actual message */
    struct msg_data place_for_data;
    memset(&place_for_data, 0, sizeof(place_for_data));
    struct iovec iov = {
        .iov_base = &place_for_data,
        .iov_len = sizeof(struct msg_data)
    };
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;

    /* prepare space to read filedescriptors */
    char cmsg[FDS_MAX_COUNT*CMSG_SPACE(sizeof(int))+CMSG_SPACE(sizeof(struct ucred))];
    msg.msg_control = cmsg;
    msg.msg_controllen = sizeof(cmsg);

    /* Receive real plus ancillary data */
    int len = recvmsg(unixsocketfd, &msg, 0);
    if (len == -1) {
        fprintf(stderr, "libzedr restart server: failed to receive message over the socket: %s\n", strerror(errno));
        return -1;
    }

    /* read message content */
    if (len != sizeof(struct msg_data)) return -1;
    if (place_for_data.magic != PROTOCOL_MAGIC) return -1;
    *received = fd_exchange_msg_init_one_dimensional(place_for_data.arguments, place_for_data.arguments_len);
    if (*received == NULL) return -1;
    pid_t pid = -1;

    /* read the ancillary data */
    struct cmsghdr *cmsgp = CMSG_FIRSTHDR(&msg);
    while (cmsgp != NULL) {
        switch (cmsgp->cmsg_type) {
            case SCM_RIGHTS: {
                assert(cmsgp->cmsg_level == SOL_SOCKET);
                assert(cmsgp->cmsg_len == CMSG_LEN(sizeof(int)*place_for_data.fd_count));

                /* read, move and store the fds */
                int fds[FDS_MAX_COUNT];
                memcpy(fds, CMSG_DATA(cmsgp), sizeof(int)*place_for_data.fd_count);
                for (uint32_t y = 0; y < place_for_data.fd_count; y++) {
                    int fd = move_fd_above(fds[y], MIN_STORED_FD);
                    fd_exchange_msg_add(*received, place_for_data.fd_ids[y], fd);
                }

                break;
            }
            case SCM_CREDENTIALS: {
                assert(cmsgp->cmsg_len == CMSG_LEN(sizeof(struct ucred)));
                assert(cmsgp->cmsg_level == SOL_SOCKET);

                struct ucred cred;
                memcpy(&cred, CMSG_DATA(cmsgp), sizeof(cred));
                pid = cred.pid;
            }
        }
        
        cmsgp = CMSG_NXTHDR(&msg, cmsgp);
    }

    /* test that we talked with the right process */
    if (pid != service_pid) {
        /* somebody is doing something nasty => abort */
        int fd;
        while ((fd = fd_exchange_msg_pop(*received, NULL)) != -1) {
            close(fd);
        }
        fd_exchange_msg_destroy(*received);
        fprintf(stderr, "libzedr: expected to communicate with PID=%d, but PID=%d sent a message instead\n", service_pid, pid);
        return -1;
    }

    return 0;
}

int fd_exchange_msg_get_fd_count(fd_exchange_msg_t *msg) {
    return msg->fd_count;
}

