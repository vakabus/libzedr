#ifndef _FD_EXCHANGE_PROTOCOL_H
#define _FD_EXCHANGE_PROTOCOL_H

#include <stdbool.h>
#include <stdint.h>

/** Minimal FD number that can be used to store the FD after receiving. */
#define MIN_STORED_FD 1000

/** Opaque type storing all data passed to the restart server */
typedef struct fd_exchange_msg fd_exchange_msg_t;

/**
 * Create an exchange message that can be send to restart server.
 * 
 * @param argc number of arguments
 * @param argv argument vector that the service should be started with
 * @return The message or NULL in case of an allocation failure.
 **/
fd_exchange_msg_t* fd_exchange_msg_create(int argc, char** argv);

/**
 * Free resources allocated by the exchange message.
 * 
 * @param msg Exchange message that should be cleaned up. Allows for NULL for conviniency.
 **/
void fd_exchange_msg_destroy(fd_exchange_msg_t *msg);

/**
 * Add an file descriptor with an ID to the exchange message
 * 
 * @param msg Exchange message to which the file descriptor should be appended.
 * @param id File descriptor identifier.
 * @param fd File descriptor (must not be FD_CLOEXEC)
 * @return 0 on success, -1 on memory allocation failure
 **/
int fd_exchange_msg_add(fd_exchange_msg_t *msg, uint64_t id, int fd);

/**
 * Pop an file descriptor from the provided exchange message.
 * 
 * @param msg Exchange message from which one file descriptor should be removed.
 * @param id Where should the file descriptor ID be stored. If NULL, ID is ignored.
 * @return File descriptor. When -1, there are no more file descriptors in the message. Returned FD is never lower than MIN_STORED_FD
 **/
int fd_exchange_msg_pop(fd_exchange_msg_t *msg, uint64_t *id);

/**
 * Get number of FDs stored in this message.
 * 
 * @param msg Exchange message which file descriptors should be counted.
 * @return Number of FDs stored.
 **/
int fd_exchange_msg_get_fd_count(fd_exchange_msg_t *msg);

/**
 * Get the stored argument vector.
 * 
 * @param msg Exchange message that is source of the arguments.
 * @param argv Place where to store the argument vector.
 * @return Argument count. -1 on allocation failure.
 **/
int fd_exchange_msg_get_argv(fd_exchange_msg_t *msg, char*** argv);

/**
 * Send the exchange message over provided unix socket FD'
 * 
 * @param unixsocketfd Unix socket FD to send the message over
 * @param msg Message to send
 * @return 0 on success, -1 on error
*/
int fd_exchange_send(int unixsocketfd, fd_exchange_msg_t *msg);

/**
 * Receive a message from the provided unix socket FD. Timeout after the given timeout. Checks PID of the sending process,
 * fails if it does not match. 
 * 
 * @param unixsocketfd Unix socket FD to receive the data from.
 * @param msg Place to store the new message. Must be freed when no longer needed.
 * @param service_pid PID of the process that we are restarting.
 * @param timeout_ms Maximum number of milliseconds we should wait for the data
 * @return 0 on success, -1 on failure (timeout, socket failure or somebody sends something unexpected)
 **/
int fd_exchange_recv(int unixsocketfd, fd_exchange_msg_t **msg, pid_t service_pid, uint32_t timeout_ms);

#endif
