#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <err.h>
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <limits.h>
#include <pthread.h>
#include <assert.h>
#include <string.h>
#include <sched.h>
#include <stdatomic.h>

#include "common.h"
#include "libzedr.h"
#include "restart_server.h"
#include "fd_exchange_protocol.h"
#include "utils.h"
#include "config.h"

extern char** environ;

/** 
 * FDs that will be passed on when restarting. The data structure is allocated in
 * zedr_init() and it is never freed.
 */
static fd_exchange_msg_t* persisted_fds = NULL;

/** Signal used for restarting */
static int restart_signal = ZEDR_DEFAULT_SIGNAL;

/** Restart server socket base dir. */
static char* restart_socket_basedir = NULL;

/** Gracefull shutdown function provided by the service */
static zedr_graceful_shutdown_f graceful_shutdown_func = NULL;
static void* graceful_shutdown_arg = NULL;

static bool is_gracefull_shutdown_configured() {
    return graceful_shutdown_func != NULL;
}

/** Mutex guarding restart in progress, so that no two threads try to restart at once */
static pthread_mutex_t restart_point_mutex = PTHREAD_MUTEX_INITIALIZER;

/** Stored state for standalone mode restarts */
static restart_server_state_t* restart_server_state = NULL;

static bool is_library_initialized() {
    return persisted_fds != NULL;
}

/** IDs of retrieved FDs. First FD is 3, then 4, .. */
static uint64_t* retrieved_fd_ids = NULL;
static int retrieved_fd_ids_size = 0;

/** Flag that we should restart. Set by signal handler. */
static volatile bool restart_pending = false;

static void signal_handler(int UNUSED(signo)) {
    restart_pending = true;
}

/* installs signal handler for initializing restarts */
static void install_signal_handler() {
    struct sigaction psa;
    psa.sa_handler = signal_handler;
    psa.sa_flags = 0; // don't restart interruptible syscalls
    sigemptyset(&psa.sa_mask);
    sigaction(restart_signal, &psa, NULL);
}

static void retrieve_fds_from_env() {
    /* validate env */
    char* s = getenv("LISTEN_PID");
    if (s == NULL) return;
    errno = 0;
    int r = strtol(s, NULL, 10);
    if (errno != 0 || r != getpid()) return;

    /* get number of FDs */
    s = getenv("LISTEN_FDS");
    if (s == NULL) goto bailout;
    errno = 0;
    r = strtol(s, NULL, 10);
    if (errno != 0 || r == 0) goto bailout;
    int number_of_fds = r;

    /* create place to store the retrieved FDs */
    retrieved_fd_ids = calloc(sizeof(uint64_t), number_of_fds);
    if (retrieved_fd_ids == NULL) goto bailout;

    /* read and store IDs */
    s = getenv("LISTEN_FDNAMES");
    if (s == NULL) goto bailout;
    retrieved_fd_ids_size = 0;
    for (int i = 0; i < number_of_fds; i++) {
        if (*s >= '0' && *s <= '9') {
            /* parse number */
            errno = 0;
            uint64_t id = strtoll(s, NULL, 10);
            if (errno != 0) goto bailout;

            /* save for further use */
            retrieved_fd_ids[i] = id;
        } else {
            goto bailout;
        }

        /* advance pointer */
        while (*s != ':' && *s != '\0') s++;
        if (*s == ':') s++;
    }
    assert(*s == '\0');

    retrieved_fd_ids_size = number_of_fds;

    return;
bailout:

    printf("libzedr: There was probably an attempt to pass us some FDs, but the retrieval failed.\n");
    if (retrieved_fd_ids != NULL) {
        free(retrieved_fd_ids);
        retrieved_fd_ids = NULL;
    }
    retrieved_fd_ids_size = 0;
    return;
}

/**
 * Connect to ZEDR restart server and send the provided FD to it.
 **/
static bool send_fd_to_restart_server(fd_exchange_msg_t* msg) {
    int res;
    bool success = true;

    /* prepare socket */
    int socketfd = socket(AF_UNIX, SOCK_DGRAM, 0);
    if (socketfd == -1) {
        fprintf(stderr, "libzedr error: failed to create socket: %s\n", strerror(errno));
        return false;
    }

    /* bind to address */
    struct sockaddr_un addr;
    addr.sun_family = AF_UNIX;
    int written = snprintf(
        addr.sun_path,
        sizeof(addr.sun_path),
        "%s" ZEDR_SOCKET_NAME_PATTERN,
        restart_socket_basedir == NULL ? ZEDR_DEFAULT_SOCKET_BASEDIR : restart_socket_basedir,
        getpid()
    );
    assert(written < ((int)sizeof(addr.sun_path)));

    /* connect to server */
    res = connect(socketfd, (struct sockaddr*) &addr, sizeof(addr));
    if (res == -1) {
        success = false;
        goto cleanup;
    };

    /* finally send it */
    success = (fd_exchange_send(socketfd, msg) == 0);

cleanup:
    shutdown(socketfd, SHUT_RDWR);
    close(socketfd);
    return success;
}

static bool repeatedly_try_to_send_fd(fd_exchange_msg_t* msg) {
    /* get current time to check for timeout */
    struct timespec time_start;
    int res = clock_gettime(CLOCK_MONOTONIC, &time_start);
    if (res == -1) err(EXIT_FAILURE, "libzedr: clock_gettime"); // should never happen => no problem failing hard

    while (true) {
        /* try to send it */
        if (send_fd_to_restart_server(msg)) return true;

        /* check timeout */
        struct timespec time_curr;
        res = clock_gettime(CLOCK_MONOTONIC, &time_curr);
        if (res == -1) err(EXIT_FAILURE, "clock_gettime");
        uint64_t elapsed_ms = (time_curr.tv_nsec - time_start.tv_nsec) / 1e6  + (time_curr.tv_sec - time_start.tv_sec) * 1000;
        if (elapsed_ms > TIMEOUT) {
            fprintf(stderr, "libzedr error: timeout connecting to restart server\n");
            return false;
        }

        poll(NULL, 0, 10); //10 ms sleep
    }
}

void zedr_config_set_restart_signal(int signum) {
    /* this method must be called before zedr_init() */
    assert(!is_library_initialized() && "Restart signal can be set only before library initialization.");

    restart_signal = signum;
}


void zedr_config_enable_custom_graceful_shutdown(const zedr_graceful_shutdown_f func, void *arg) {
    assert(!is_gracefull_shutdown_configured() && "Gracefull shutdown can be configured only once.");

    graceful_shutdown_func = func;
    graceful_shutdown_arg = arg;
}

void zedr_config_set_restart_socket_dir(char* dirname) {
    assert(restart_socket_basedir == NULL && "Socket directory can be set only once.");

    restart_socket_basedir = strdup(dirname);
}


void zedr_init(int argc, char** argv) {
    /* must not be called twice */
    assert(!is_library_initialized() && "Initialization must be called only once");

    /* initialise the data structure storing FDs for later reuse */
    char** nargv = strarray_dup(argv, argc+1); // copy the last null too
    persisted_fds = fd_exchange_msg_create(argc, nargv);
    assert(persisted_fds != NULL && "Out of memory, libzedr initialization failed.");

    /* save state to allow restoration in the standalon restart server */
    restart_server_state = restart_server_state_snapshot();

    /* try to obtain FDs from environment in case we are running after a restart */
    retrieve_fds_from_env();

    /* enable restart notifications */
    install_signal_handler();
}

int zedr_fd_init_with_id(const zedr_init_socket_f func, void* arg, uint64_t id) {
    assert(is_library_initialized());

    /* obtain the FD */
    int fd = -1;
    if (retrieved_fd_ids == NULL) {
        /* get a new */
        fd = func(arg);
    } else {
        /* reuse previously created FD */
        for (int i = 0; i < retrieved_fd_ids_size; i++) {
            if (retrieved_fd_ids[i] == id) {
                fd = i+3; // because the first FD is 3
                break;
            }
        }
    }

    assert(persisted_fds != NULL);
    fd_exchange_msg_add(persisted_fds, id, fd);
    return fd;
}

int zedr_fd_init(const zedr_init_socket_f func, void* arg) {
    return zedr_fd_init_with_id(func, arg, ZEDR_DEFAULT_FD_ID);
}

void zedr_restart_point() {
    assert(is_library_initialized());

    if (!restart_pending) return;

    /* make sure there is only a single thread handling the restart */
    int res = pthread_mutex_trylock(&restart_point_mutex);
    if (res != 0) {
        /* lock is already locked, so the restart is already being handled by other thread
           so this thread has nothing to do */

        /* when gracefull shutdown function was set, we do not terminate threads */
        if (!is_gracefull_shutdown_configured())
            pthread_exit(NULL);
    }

    fprintf(stderr, "libzedr: restart point triggered, starting restart procedure\n");
    if (fd_exchange_msg_get_fd_count(persisted_fds) == 0)
        fprintf(stderr, "libzedr warning: no file descriptors registered, transparent restart impossible\n");
    
    /* try to send the fd blindly */
    bool success = send_fd_to_restart_server(persisted_fds);
    if (!success) {
        fprintf(stderr, "libzedr: spawning restart server as a child\n");
        /* expect that nobody is listening, so spawn own restart server */
        restart_server_start(restart_server_state, restart_socket_basedir);
        /* and send it there */
        repeatedly_try_to_send_fd(persisted_fds); 
    }

    /* terminate service execution */
    if (is_gracefull_shutdown_configured())
        graceful_shutdown_func(graceful_shutdown_arg);
    else {
        /* signal all other threads (all processes in this process group)
           to interrupt blocking syscalls in every one of them*/
        signal_all_other_threads(restart_signal);

        /* terminate this thread */
        pthread_exit(NULL);
    }
}

static void wait_for_data_restart_point(int fd) {
    /* wait for any data to become available
    
       uses zedr_poll() so that when it returns, we know there are data
       available and that the signals were handled correctly while waiting */

    struct pollfd pollfd = {
        .fd = fd,
        .events = POLLIN,
        .revents = 0
    };
    (void) zedr_poll(&pollfd, 1, -1);
}

int zedr_accept(int socket, struct sockaddr *restrict addr, socklen_t *restrict address_len) {
    assert(is_library_initialized());

    wait_for_data_restart_point(socket);
    int res = accept(socket, addr, address_len);
    if (res == -1 && errno == EINTR) zedr_restart_point();
    return res;
}

int zedr_poll(struct pollfd fds[], nfds_t nfds, int timeout) {
    assert(is_library_initialized());

    /* block all signals to prevent the sleeping barber problem */
    sigset_t block_all;
    sigset_t original;
    sigfillset(&block_all);
    pthread_sigmask(SIG_BLOCK, &block_all, &original);

    /* test whether signal was received before we blocked */
    zedr_restart_point();

    /* poll */
    struct timespec time = {
        .tv_sec = timeout / 1000,
        .tv_nsec = (timeout % 1000) * 1000000
    };
    struct timespec* t = timeout == -1 ? NULL : &time; // unlimited wait
    int rv = ppoll(fds, nfds, t, &original);

    /* unconditional restart point checking for a signal received during 
       ppoll() runtime
       poll() does not change the FDs state and it is therefore still safe
       to stop the thread even when poll() returned valid data
    */
    zedr_restart_point();

    /* unblock signals */
    pthread_sigmask(SIG_SETMASK, &original, NULL);

    return rv;
}

ssize_t zedr_read(int fd, void* buf, size_t count) {
    assert(is_library_initialized());

    wait_for_data_restart_point(fd);
    ssize_t size = read(fd, buf, count);
    if (size == -1 && errno == EINTR) zedr_restart_point();
    return size;
}

ssize_t zedr_recv(int sockfd, void *buf, size_t len, int flags) {
    assert(is_library_initialized());

    wait_for_data_restart_point(sockfd);
    ssize_t size = recv(sockfd, buf, len, flags);
    if (size == -1 && errno == EINTR) zedr_restart_point();
    return size;
}
