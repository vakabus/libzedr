#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <err.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <errno.h>
#include <poll.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <signal.h>
#include <assert.h>
#include <dirent.h>

#include <libzedr.h>
#include <libzedr-sm.h>

#include "restart_server.h"
#include "common.h"
#include "fd_exchange_protocol.h"
#include "utils.h"

struct restart_server_state {
    pid_t pid_of_process_to_restart;
    char* work_dir;
    int saved_stdin_fd;
    int saved_stdout_fd;
    int saved_stderr_fd;
    char** env;
    char* restart_server_socket_basedir;
    sigset_t signal_mask;
};

static bool state_initialized = false;
static restart_server_state_t state;

restart_server_state_t* restart_server_state_snapshot() {
    /* it does not make sense to snapshot state more than once
       and also we can prevent additional allocation by allocating 
       everything statically. Moreover, we avoid allocation errors.
    */
    assert(!state_initialized);
    state_initialized = true;

    state.saved_stderr_fd = move_fd_above(STDERR_FILENO, 100);
    state.saved_stdin_fd = move_fd_above(STDIN_FILENO, 101);
    state.saved_stdout_fd = move_fd_above(STDOUT_FILENO, 102);
    dup2(state.saved_stderr_fd, STDERR_FILENO);
    dup2(state.saved_stdin_fd, STDIN_FILENO);
    dup2(state.saved_stdout_fd, STDOUT_FILENO);

    int environ_len = 0;
    while (environ[environ_len] != NULL) environ_len++;
    state.env = strarray_dup(environ, environ_len+1); // copy the last null

    state.work_dir = get_current_dir_name();

    pthread_sigmask(SIG_SETMASK, NULL, &state.signal_mask);

    return &state;
}

static void restore_cwd(restart_server_state_t* args) {
    int res = chdir(args->work_dir);
    if (res == -1) err(1, "chdir");
}

static void restore_signal_mask(restart_server_state_t* state) {
    int r = pthread_sigmask(SIG_SETMASK, &state->signal_mask, NULL);
    if (r != 0) errx(1, "pthread_sigmask: %s", strerror(r));
}

/** Close all FDs. Restore standard io that was saved at the program start. */
static void restore_standard_io(restart_server_state_t* args) {
    /* first move the saved stdio FDs to the standard location*/
    dup2(args->saved_stderr_fd, STDERR_FILENO);
    dup2(args->saved_stdout_fd, STDOUT_FILENO);
    dup2(args->saved_stdin_fd, STDIN_FILENO);

    /* then close everything else */
    DIR *d;
    struct dirent *dir;
    d = opendir("/proc/self/fd");
    assert(d != NULL);

    while ((dir = readdir(d)) != NULL) {
        char* dirname = dir->d_name;
        
        errno = 0;
        int fd = strtol(dirname, NULL, 10);
        assert(errno == 0);

        switch (fd) {
            case STDERR_FILENO:
            case STDOUT_FILENO:
            case STDIN_FILENO:
                break;
            default:
                close(fd);
        }
    }
    closedir(d);
}

static void restore_env(restart_server_state_t* args) {
    clearenv();
    for (int i = 0; args->env[i] != NULL; i++)
        putenv(args->env[i]);
}

static void prepare_env_for_child(fd_exchange_msg_t* msg) {
    char small_buff[22]; // must fit one id and a prefix
    char buff[1024]; // must fit all ids separated by commas
    memset(buff, 0, sizeof(buff));

    /* save current pid so that the new service knows it's its (man SD_LISTEN_FDS) */
    snprintf(small_buff, sizeof(small_buff), "%d", getpid());
    setenv("LISTEN_PID", small_buff, 1);

    /* save number of FDs being passed */
    snprintf(small_buff, sizeof(small_buff), "%d", fd_exchange_msg_get_fd_count(msg));
    setenv("LISTEN_FDS", small_buff, 1);

    /* because received FDs have values larger than MIN_STORED_FD and
       the restart server was started with just the first basic 3 FDs,
       we can safely move the FDs to 3,4,.. to match systemd's behaviour */

    int dest_fd = 3; // SD_LISTEN_FDS_START in <systemd/sd-daemon.h>
    int fd;
    uint64_t id;
    while ((fd = fd_exchange_msg_pop(msg, &id)) != -1) {
        bool succ = move_fd_to(fd, dest_fd);
        assert(succ); // should work, because all FDs except for the <3 should be now closed
        dest_fd++;

        (void) snprintf(small_buff, sizeof(small_buff), ":%ld", id); // convert the number to string with colon prefix
        (void) strncat(buff, small_buff, sizeof(buff)); // append the colon and number
    }
    setenv("LISTEN_FDNAMES", buff+1, 1); // buff+1 because we are stripping one leading colon
}

static void fd_passing_socket_destroy(int socketfd, pid_t pid_of_restarted_process, char* server_socket_basedir) {
    /* cleanup files left behind */
    char buff[sizeof(struct sockaddr_un)]; // we know the name fits in that size
    int written = snprintf(
        buff,
        sizeof(buff),
        "%s" ZEDR_SOCKET_NAME_PATTERN,
        server_socket_basedir == NULL ? ZEDR_DEFAULT_SOCKET_BASEDIR : server_socket_basedir,
        pid_of_restarted_process);
    assert(written < (int)sizeof(buff));
    unlink(buff); // can fail and we dont care

    /* close the socket */
    close(socketfd); // can fail and we dont care
}

static int fd_passing_socket_init(pid_t pid_of_restarted_process, char* restart_server_basedir) {
    /* open socket */
    int socketfd = socket(AF_UNIX, SOCK_DGRAM, 0);
    if (socketfd == -1) goto fail;

    struct sockaddr_un control_socket_addr;
    control_socket_addr.sun_family = AF_UNIX;
    int written = snprintf(
        control_socket_addr.sun_path,
        sizeof(control_socket_addr.sun_path),
        "%s" ZEDR_SOCKET_NAME_PATTERN,
        restart_server_basedir == NULL ? ZEDR_DEFAULT_SOCKET_BASEDIR : restart_server_basedir,
        pid_of_restarted_process);
    assert(written < (int)sizeof(control_socket_addr.sun_path));

    int res = listen(socketfd, 1);
    if (res != -1) goto fail;

    mode_t old = umask(0); // make the socket accessible by anyone (we are checking PID of the sender, so its not a security issue)
    res = bind(socketfd, (struct sockaddr *)&control_socket_addr, sizeof(control_socket_addr));
    umask(old);
    if (res == -1) goto fail;

    /* set that we want to send credentials over the socket to verify sender */
    int t = (int) true;
    res = setsockopt(socketfd, SOL_SOCKET, SO_PASSCRED, &t, sizeof(t));
    if (res == -1) goto fail;

    return socketfd;

fail:
    fprintf(stderr, "libzedr: error while creating listen socket for restart server - %s\n", strerror(errno));
    fd_passing_socket_destroy(socketfd, pid_of_restarted_process, restart_server_basedir);
    return -1;
}

__attribute__((noreturn)) static int exec_new_service_instance(char** argv) {
    execv(argv[0], argv);

    /* print the arguments we used */
    fprintf(stderr, "libzedr: error while trying to exec the new instance using these arguments:\n");
    int i = 0;
    while (argv[i] != NULL) {
        fprintf(stderr, "\t%s\n", argv[i]);
        i++;
    }

    err(EXIT_FAILURE, "execv"); /// if we get here it always is an error
}

__attribute__((noreturn)) static void restart_server_main(restart_server_state_t *state) {
    /* restore state to the same as when this program first started */
    restore_standard_io(state);
    restore_cwd(state);
    restore_env(state);
    restore_signal_mask(state);


    /* start listening for connections */
    int socketfd = fd_passing_socket_init(state->pid_of_process_to_restart, state->restart_server_socket_basedir);
    if (socketfd < 0) err(1, "restart server: socket init");

    /* receive the FD */
    fd_exchange_msg_t *msg;
    int r = fd_exchange_recv(socketfd, &msg, state->pid_of_process_to_restart, TIMEOUT);
    fd_passing_socket_destroy(socketfd, state->pid_of_process_to_restart, state->restart_server_socket_basedir);
    if (r < 0) err(1, "fd_recv");

    /* pass FDs to future child */
    prepare_env_for_child(msg);

    /* start new service */
    char** argv = NULL;
    fd_exchange_msg_get_argv(msg, &argv);
    exec_new_service_instance(argv);

    /* this code is never reached => cleanup is not required */
}

void restart_server_start(restart_server_state_t *args, char* socket_basedir) {
    /* save additional information to the state just before forking */
    args->pid_of_process_to_restart = getpid();
    args->restart_server_socket_basedir = socket_basedir;

    pid_t pid = fork();
    switch (pid) {
        case -1: err(EXIT_FAILURE, "fork");
        case 0: {
            /* child */
            restart_server_main(args);
            assert(false); // should never happen
        }
        default: {
            /* parent */
            return;
        }
    }
}

int zedr_sm_server_run(pid_t process_pid, int restart_signum, char* server_socket_basedir, int timeout_ms, int *fds[], uint64_t *ids[]) {
    /* initialize socket */
    int socketfd = zedr_sm_server_init(process_pid, restart_signum, server_socket_basedir);
    if (socketfd < 0) return -1;

    return zedr_sm_server_process(socketfd, process_pid, server_socket_basedir, timeout_ms, fds, ids);
}


int zedr_sm_server_init(pid_t process_pid, int restart_signum, char* server_socket_basedir) {
    /* start listening for connections */
    int socketfd = fd_passing_socket_init(process_pid, server_socket_basedir);
    if (socketfd < 0) return -1;

    /* notify the service about restart */
    int r = kill(process_pid, restart_signum == 0 ? ZEDR_DEFAULT_SIGNAL : restart_signum);
    if (r < 0) {
        close(socketfd);
        return -2;
    }

    return socketfd;
}


int zedr_sm_server_process(int socketfd, pid_t process_pid, char* server_socket_basedir, int timeout_ms, int *fds[], uint64_t *ids[]) {
    /* receive the FD */
    fd_exchange_msg_t *msg = NULL;
    int r = fd_exchange_recv(socketfd, &msg, process_pid, timeout_ms);
    fd_passing_socket_destroy(socketfd, process_pid, server_socket_basedir);
    if (r < 0) {
        return -1;
    }

    /* store fds in the returned array */
    int count = fd_exchange_msg_get_fd_count(msg);
    *fds = malloc(sizeof(**fds) * count);
    *ids = malloc(sizeof(**ids) * count);
    for (int i = 0; i < count; i++) {
        (*fds)[i] = fd_exchange_msg_pop(msg, &((*ids)[i]));
        assert((*fds)[i] >= 0);
    }

    /* cleanup */
    fd_exchange_msg_destroy(msg);

    return count;
}

void zedr_sm_server_cleanup(int control_fd, pid_t process_pid, char* server_socket_basedir) {
    fd_passing_socket_destroy(control_fd, process_pid, server_socket_basedir);
}
