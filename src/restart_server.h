#ifndef _RESTART_SERVER_H
#define _RESTART_SERVER_H

#include <stdbool.h>
#include <stdint.h>
#include <unistd.h>


typedef struct restart_server_state restart_server_state_t;

/**
 * Create a snapshot of current state and return it for future
 * use by the restart server.
 **/
restart_server_state_t* restart_server_state_snapshot();

/**
 * Starts restart server in a separate process.
 * 
 * @param args Arguments for the restart server.
 **/
void restart_server_start(restart_server_state_t *args, char* socket_basedir);

#endif
