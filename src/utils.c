#define _DEFAULT_SOURCE

#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <malloc.h>
#include <assert.h>
#include <dirent.h>
#include <sys/syscall.h>
#include <errno.h>
#include <stdlib.h>

#include "utils.h"

static bool is_allocated_fd(int fd) {
    int r = fcntl(fd, F_GETFD);
    return r >= 0;
}

bool move_fd_to(int from, int to) {
    /* check that the destination fd is free */
    if (is_allocated_fd(to)) return false;

    int r = dup2(from, to);
    if (r < 0) return false;

    close(from);
    return true;
}

int move_fd_above(int fd, int min) {
    assert(is_allocated_fd(fd));
    if (fd >= min) return fd;

    /* find first free FD */
    int new_fd = min;
    while (!move_fd_to(fd, new_fd)) {
        new_fd++;
    }

    return new_fd;
}

char** strarray_dup(char** arr, int len) {
    char** res = malloc(len*sizeof(char*));
    if (res == NULL) return NULL;

    for (int i = 0; i < len; i++) {
        if (arr[i] == NULL)
            res[i] = NULL;
        else 
            res[i] = strdup(arr[i]);
    }
    return res;
}

void signal_all_other_threads(int signum) {
    DIR *d;
    struct dirent *dir;
    d = opendir("/proc/self/task");
    if (d) {
        pid_t mytid = syscall(SYS_gettid);
        while ((dir = readdir(d)) != NULL) {
            errno = 0;
            pid_t tid = (pid_t) strtol(dir->d_name, NULL, 10);
            assert(errno == 0);
            if (tid != mytid)
                syscall(SYS_tgkill, getpid(), tid, signum);
        }
        closedir(d);
    } else {
        assert(false && "Can't have a process without threads");
    }
}
