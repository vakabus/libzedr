#ifndef _UTILS_H
#define _UTILS_H

#include <stdbool.h>

/**
 * Try to move FD to another value. Returns true when success.
 **/
bool move_fd_to(int from, int to);

/**
 * Move FD above given minimum. Returns the new one.
 **/
int move_fd_above(int fd, int min);

/**
 * Copy strarray to a newly allocated space.
 **/
char** strarray_dup(char** arr, int len);

/**
 * Signal all threads in this thread group.
 **/
void signal_all_other_threads(int signum);


#endif
