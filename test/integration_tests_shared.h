/**
 * @file integration_tests_shared.h
 * 
 * This file contains shared code for integration tests with a test service
 * that serves primes.
 **/

#pragma once

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include <err.h>
#include <time.h>
#include <stdatomic.h>
#include <string.h>
#include <errno.h>
#include <stddef.h>

#include <libzedr.h>

#define MIN_QUERY_SIZE 800
#define MAX_QUERY_SIZE 1200

#define LISTEN_PORT 5555
#define MAX_INPUT_SIZE 128

#define assert_ret( cond, ret, msg ) if (! ( cond )) {fprintf(stderr, "libzedr test failure: '%s' evaluates to false, msg '%s'\n", #cond, #msg); return ret;}

_Pragma("GCC diagnostic push") _Pragma("GCC diagnostic ignored \"-Wunused-function\"")

static atomic_bool overall_success = true;

typedef bool(*request_f)(void);

static void sleep_ns(uint64_t ns) {
    struct timespec sleep_duration = {
        .tv_sec = 0,
        .tv_nsec = ns,
    };
    nanosleep(&sleep_duration, NULL);
}

static bool is_prime(const uint64_t p) {
    for (uint64_t q = p-1; q > 1; q--) {
        if (p % q == 0) {
            return false;
        }
    }

    return true;
}

static uint64_t get_nth_prime(const uint64_t n) {
    uint64_t candidate = 2;
    uint64_t count = 0;
    while (count < n) {
        if (is_prime(candidate)) {
            count++;
        }

        candidate++;
    }

    return candidate - 1;
}

static int init_server_socket(void *arg) {
    int socketfd = socket(AF_INET, SOCK_STREAM, 0);
    if (socketfd == -1) err(1, "socket");

    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(LISTEN_PORT);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);

    /* So that we can use the port immediately again after restart. */
    int optval = 1;
    if (setsockopt(socketfd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof (optval)) == -1)
        err(1, "setsockopt");

    if (bind(socketfd, (struct sockaddr *)&addr, sizeof (addr)) == -1)
        err(1, "bind");

    if (listen(socketfd, SOMAXCONN) == -1)
        err(1, "listen");
    
    printf("Accepting connections on port %d\n", LISTEN_PORT);

    return socketfd;
}

static int init_client_socket() {
    struct sockaddr_in server;
    server.sin_addr.s_addr = inet_addr("127.0.0.1");
	server.sin_family = AF_INET;
	server.sin_port = htons(LISTEN_PORT);

    int socketfd = socket(AF_INET, SOCK_STREAM, 0);
    if (socketfd == -1) err(1, "socket");

    struct timeval timeout;      
    timeout.tv_sec = 4;
    timeout.tv_usec = 0;

    if (setsockopt (socketfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout,
                sizeof(timeout)) < 0)
        err(1, "setsockopt1");

    if (setsockopt (socketfd, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout,
                sizeof(timeout)) < 0)
        err(1, "setsockopt2");

    if (connect(socketfd, (struct sockaddr*) &server, sizeof(server)) == -1) {
        close(socketfd);
        assert_ret(false, -1, "connect");
    }

    return socketfd;
}

static bool default_client() {
    int socketfd = init_client_socket();
    
    long int query = MIN_QUERY_SIZE + abs(rand()) % (MAX_QUERY_SIZE - MIN_QUERY_SIZE);

    char buff[MAX_INPUT_SIZE + 1];
    ssize_t len = (ssize_t) snprintf(buff, MAX_INPUT_SIZE, "%ld", query);

    ssize_t written = write(socketfd, buff, len);
    assert_ret(written != -1, false, "write");
    assert_ret(written == len, false, "write failed to write whole query");

    ssize_t s = read(socketfd, buff, MAX_INPUT_SIZE);
    assert_ret(s != -1, false, "read");
    buff[s] = 0;
    
    int res = shutdown(socketfd, SHUT_RDWR);
    assert_ret(res != -1, false, "shutdown");

    res = close(socketfd);
    assert_ret(res != -1, false, "close");

    return true;
}

static void signal_parent(bool success) {
    /* signal parrent */
    pid_t parent = getppid();
    int res = kill(parent, success ? SIGUSR1 : SIGUSR2);
    if (res == -1) err(1, "kill");
}

static void request(request_f func) {
    struct timespec start;
    clock_gettime(CLOCK_MONOTONIC, &start);

    bool success = func();
    signal_parent(success);
    overall_success &= success;

    struct timespec end;
    clock_gettime(CLOCK_MONOTONIC, &end);

    uint64_t millis = (end.tv_sec - start.tv_sec) * 1000 + (end.tv_nsec - start.tv_nsec) / 1e6;
    printf("%s\tin %ldms\n",  success ? "SUCESS" : "FAIL", millis);
}

static void server_handle_request(int accepted_fd) {
    int fd = accepted_fd;
    uint64_t result = 0;
    int r = 0;
    uint64_t input_number = 0;

    /* read input */
    char buff[MAX_INPUT_SIZE + 1];
    int size = read(fd, buff, MAX_INPUT_SIZE + 1);
    if (size == -1) {
        fprintf(stderr, "invalid connection attempt\n");
        goto connection_cleanup;
    }

    /* insert null-byte at the end */
    buff[size] = 0;

    /* convert input to number */
    errno = 0;
    input_number = strtoull(buff, NULL, 10);
    if (errno != 0) {
        fprintf(stderr, "connection attempt with invalid input\n");
        goto connection_cleanup;
    }

    printf("Received query for %ld-th prime...\n", input_number);

    /* calculate result */
    result = get_nth_prime(input_number);

    /* write response back to the socket */
    r = snprintf(buff, MAX_INPUT_SIZE, "%ld", result);
    if (write(fd, buff, r) == -1) err(1, "write");

    connection_cleanup: {
        int res = 0;
        res = shutdown(fd, SHUT_RDWR);
        if (res == -1) fprintf(stderr, "shutdown: %s\n", strerror(errno));

        res = close(fd);
        if (res == -1) err(1, "close");
    }
}

static void test_end() {
    exit((int) !overall_success); // return code logic is inverted
}

_Pragma("GCC diagnostic pop")
