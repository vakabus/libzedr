#include <pthread.h>

#include "../integration_tests_shared.h"


#define TESTS 40
#define THREADS 4

void* thread_main(void* arg) {
    for (int i = 0; i < TESTS / THREADS; i++) {
        request(default_client);
    }

    return NULL;
}

int main() {
    /* initialize random number generator deterministically */
    srand(0xdeadbeef);

    /* spawn threads running queries */
    pthread_t threads[THREADS];
    for (int i = 0; i < THREADS; i++) {
        pthread_create(&threads[i], NULL, thread_main, NULL);
    }

    /* wait for threads to finish */
    for (int i = 0; i < THREADS; i++)
        pthread_join(threads[i], NULL);

    test_end();
}
