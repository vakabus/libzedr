#include <pthread.h>

#include "../integration_tests_shared.h"

#define THREADS 8

void* server_thread(void* voidfd) {
    int socketfd = (int) (ptrdiff_t) voidfd;

    while(true) {
        int fd = zedr_accept(socketfd, NULL, NULL);
        if (fd == -1) err(1, "accept");

        sleep(1);
        server_handle_request(fd);
    }

    return NULL;
}

int main(int argc, char** argv) {
    zedr_init(argc, argv);
    int socketfd = zedr_fd_init(init_server_socket, NULL);

    /* create worker threads */
    pthread_t threads[THREADS];
    for (int i = 0; i < THREADS; i++) {
        pthread_create(&threads[i], NULL, server_thread, (void*) (ptrdiff_t) socketfd);
        sleep_ns(500*1000*1000);
    }

    /* wait for thread to end (never happens, but terminating main thread
       has wierd consequences */
    for (int i = 0; i < THREADS; i++) {
        pthread_join(threads[i], NULL);
    }

    return 0; // this will never get called, but makes compiler happy
}
