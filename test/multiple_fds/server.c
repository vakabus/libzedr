#define _GNU_SOURCE

#include <libzedr.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <stdbool.h>
#include <assert.h>


static bool fd1_initialized = false;
static bool fd2_initialized = false;

int fd1_init(void* arg) {
    fd1_initialized = true;
    return memfd_create("first virtual file", 0);
}

int fd2_init(void* arg) {
    fd2_initialized = true;
    return memfd_create("second virtual file", 0);
}

/**
 * Tests, that passing two between restarts works.
 **/
int main(int argc, char** argv) {
    zedr_init(argc, argv);

    (void) zedr_fd_init_with_id(fd1_init, NULL, 1);
    (void) zedr_fd_init_with_id(fd2_init, NULL, 2);

    assert(fd1_initialized == fd2_initialized);

    // if running for the first time, wait for a restart point
    if (fd1_initialized)
        while(true) {
            zedr_restart_point();
            sleep(1);
        }

    // else report success
    kill(atoi(argv[1]), SIGUSR1);
    return 0;
}
