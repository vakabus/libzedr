#!/usr/bin/python

"""
This scripts runs integration tests with a few test services.

The test procedure looks like this:
* server is started
* client starts and sends SIGUSR signals for every completed request
* after 5 SIGUSR1 signals, the server is considered started
* a restart is initiated
* after 5 SIGUSR1 signals, the server is considered restarted
* if any SIGUSR2 signal arrives (failed request), the test fails

Tests are run in standalone mode and with systemd. Systemd support
is automatically detected and skipped if it is not possible to run
the tests.

The script exits with 0 when everything succeeded.
"""

import os
import sys
import subprocess
import signal
import time
from typing import *

# ignore SIGUSR1
signal.signal(signal.SIGUSR1, signal.SIG_IGN)
signal.signal(signal.SIGUSR2, signal.SIG_IGN)

TERM_RED = '\x1b[31m'
TERM_GREEN = '\x1b[32m'
TERM_RESET = '\x1b[0m'
TERM_YELLOW = '\x1b[93m'

def get_git_root() -> str:
    return subprocess.check_output("git rev-parse --show-toplevel".split(" ")).decode('utf8')[:-1]

def get_pids(process_name: str) -> List[int]:
    try:
        res = subprocess.check_output(["pidof", process_name])
        return list(map(int, res.decode('utf8').split(" ")))
    except subprocess.CalledProcessError:
        return []

def is_running(process_name: str) -> bool:
    return get_pids(process_name) > 0

def killpids(pids: List[int], signal=signal.SIGTERM):
    for p in pids:
        os.kill(p, signal)

def killall(process_name: str, signal=signal.SIGTERM):
    pids = get_pids(process_name)
    killpids(pids, signal)

def waitall():
    try:
        while True:
            _ = os.wait()
    except ChildProcessError:
        # this exception is thrown when no child processes exist
        pass

def waitpids(pids: List[int]):
    for p in pids:
        _ = os.waitpid(p, 0)

def does_systemd_support_libzedr():
    res = subprocess.call("systemctl show dummy | grep RestartTransparently", shell=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    return res == 0

def are_we_root():
    return os.geteuid() == 0

def was_compiled_with_libsystemd():
    with open('build_dir/config.h') as f:
        for line in f:
            if "#define HAVE_LIBSYSTEMD 1" in line:
                return True
    return False

def is_systemd_the_init():
    res = subprocess.call("systemctl show dummy | cat", shell=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    return res == 0

class TestInterface(object):
    def __init__(self, name):
        self.name = name

    def print_success(self, success):
        if success:
            self.print(f"Test {TERM_GREEN}SUCCESSFUL{TERM_RESET}")
        else:
            self.print(f"Test {TERM_RED}FAILED{TERM_RESET}")

    def print(self, *vargs, **kvargs):
        space = " "*(16-len(self.name))
        print(f"{TERM_YELLOW}[{self.name}]{TERM_RESET}{space}", end="")
        print(*vargs, **kvargs)

    def test(self) -> bool:
        raise Exception("Not implemented - this is an interface")

class StandaloneRestartOnlyTest(TestInterface):
    """
    Tests, that we receive SIGUSR1 after a restart
    """

    def __init__(self, name):
        super().__init__(name)
        
        self.signal_received = True
        self.server_name = os.path.join(os.getcwd(), f"./build_dir/test/{name}/server")

    def start_server(self) -> bool:
        # spawn the server process
        proc = subprocess.Popen([self.server_name, str(os.getpid())], stdout=None)

        # check if it stays running
        time.sleep(0.2)
        return proc.poll() is None

    def restart_server(self) -> bool:
        pids = get_pids(self.server_name)
        killpids(pids, signal=signal.SIGHUP)
        return True

    def test(self) -> bool:
        self.print("Test start")

        # setup signal handler for detecting success
        def handler(_, __):
            self.signal_received = True
        signal.signal(signal.SIGUSR1, handler)

        # start and restart
        result = True
        result &= self.start_server()
        result &= self.restart_server()

        # wait for the signal to arrive
        time.sleep(0.5)
        result &= self.signal_received

        # reset signal handler
        signal.signal(signal.SIGUSR1, signal.SIG_IGN)

        # cleanup
        killall(self.server_name, signal.SIGKILL)
        waitall()

        return result

class SystemdRestartOnlyTest(TestInterface):
    """
    Tests, that we receive SIGUSR1 after a restart
    """

    service_name = 'libzedr_systemd_test.service'
    service_file = f'/etc/systemd/system/{service_name}'

    def __init__(self, name):
        super().__init__(name)
        
        self.signal_received = True
        self.server_name = os.path.join(os.getcwd(), f"./build_dir/test/{name}/server")

    def start_server(self) -> bool:
        with open(SystemdRestartOnlyTest.service_file, 'w') as f:
            f.write(f"""
[Unit]
Description=Libzedr test prime server

[Service]
ExecStart={self.server_name} {os.getpid()}
Restart=always
RestartTransparently=true
""")

        res = True
        res &= (subprocess.call("systemctl daemon-reload", shell=True) == 0)
        res &= (subprocess.call(f"systemctl start {SystemdTest.service_name}", shell=True) == 0)
        return res

    def restart_server(self) -> bool:
        return subprocess.call(f"systemctl restart {SystemdTest.service_name}", shell=True) == 0

    def stop_server(self):
        res = True
        res &= (subprocess.call(f"systemctl stop {SystemdTest.service_name}", shell=True) == 0)
        os.unlink(SystemdRestartOnlyTest.service_file)
        return res

    def test(self) -> bool:
        # condition checks
        if not does_systemd_support_libzedr():
            self.print("Skipped - systemd support not detected")
            return None
        if not are_we_root():
            self.print("Skipped - we are not root")
            return None

        self.print("Test start")

        # setup signal handler for detecting success
        def handler(_, __):
            self.signal_received = True
        signal.signal(signal.SIGUSR1, handler)

        # start and restart
        result = True
        result &= self.start_server()
        result &= self.restart_server()

        # wait for the signal to arrive
        time.sleep(0.5)
        result &= self.signal_received

        # reset signal handler
        signal.signal(signal.SIGUSR1, signal.SIG_IGN)

        # cleanup
        result &= self.stop_server()

        return result


class Test(TestInterface):
    """
    This is generic test infrastracture for testing whether one restart 
    introduces a downtime into a service.

    Not directly used.
    """
    def __init__(self, name, restarts=1):
        super().__init__(name)

        self.request_count = 0  # total number of requests to the server
        self.request_count_failed = 0   # number of failed requests
        self.restarts = restarts
        self.server_name = os.path.join(os.getcwd(), f"./build_dir/test/{name}/server")
        self.client_name = os.path.join(os.getcwd(), f"./build_dir/test/{name}/client")
    
    def enable_signal_handling(self):
        def handler(signum, _):
            if signum == signal.SIGUSR1:
                self.request_count += 1
            elif signum == signal.SIGUSR2:
                self.request_count += 1
                self.request_count_failed += 1
            else:
                assert False, "unexpected signal"
        signal.signal(signal.SIGUSR1, handler)
        signal.signal(signal.SIGUSR2, handler)
    
    def disable_signal_handler(self):
        signal.signal(signal.SIGUSR1, signal.SIG_IGN)
        signal.signal(signal.SIGUSR2, signal.SIG_IGN)
    
    def start_client(self) -> bool:
        # start client
        proc = subprocess.Popen([self.client_name], stdout=None)

        # check if it stays running
        time.sleep(0.2)
        return proc.poll() is None
    
    def stop_client(self) -> bool:
        # terminate
        pids = get_pids(self.client_name)
        killpids(pids)
        waitpids(pids)

        # validate
        time.sleep(0.2)
        pids = get_pids(self.client_name)
        return len(pids) == 0
    
    def test(self) -> bool:
        success = True

        def wait_for_five(success) -> bool:
            """
            Waits for 5 successfull requests. Timeouts if there is a gap 
            between requests larger than 5 secs.
            """
            start_timestamp = time.time()
            original_request_count = self.request_count
            b = self.request_count
            while success and self.request_count - original_request_count < 5:
                time.sleep(0.2)
                if b < self.request_count:
                    b = self.request_count
                    start_timestamp = time.time()
                if time.time() - start_timestamp > 5:
                    success = False
            return success

        self.enable_signal_handling()
        # variable self.request_count now tracks number of successful requests made by test client

        # start the test processes
        self.print("Test start")
        success &= self.start_server()
        time.sleep(0.1) # to prevent race conditions where the client runs first
        success &= self.start_client()

        # wait for 5 successfull requests or timeout after 5sec without any response
        success = wait_for_five(success)
            
        success &= (self.request_count_failed == 0)
        
        if not success:
            self.print("Failed before even attempting restart...")

        for i in range(self.restarts):
            # restart server
            if success:
                self.print(f"Restart {i}")
                success &= self.restart_server()

            if not success:
                self.print("Failed restart")

            # wait for 5 successfull requests or timeout after 5sec with no response
            success = wait_for_five(success)

            if not success:
                self.print("Failed recovery after restart")
                break

        # stop the test
        if not self.stop_client():
            self.print("Failed to stop the client.")
            success = False
        if not self.stop_server():
            self.print("Failed to stop the server.")
            success = False

        self.disable_signal_handler()
        self.print("Test end")

        if self.request_count_failed > 0:
            self.print("Some requests after restart failed. :(")
            success = False

        self.print_success(success)
        return success


    def start_server(self) -> bool:
        pass

    def restart_server(self) -> bool:
        pass

    def stop_server(self) -> bool:
        pass

class StandaloneTest(Test):
    """
    This simple test runs services as a child of the Python interpreter and tests
    whether a restart introduces a downtime.
    """
    def __init__(self, name, signal=signal.SIGHUP, **kvargs):
        super(StandaloneTest, self).__init__(name, **kvargs)

        self.restart_signal = signal

    def start_server(self) -> bool:
        # spawn the server process
        proc = subprocess.Popen([self.server_name], stdout=None)

        # check if it stays running
        time.sleep(0.2)
        return proc.poll() is None

    def restart_server(self) -> bool:
        pids = get_pids(self.server_name)
        killpids(pids, signal=self.restart_signal)
        return True

    def stop_server(self):
        # terminate
        pids = get_pids(self.server_name)
        killpids(pids)
        waitall()

        # validate
        time.sleep(0.2)
        pids = get_pids(self.server_name)
        return len(pids) == 0
    

class SystemdTest(Test):
    """
    This simple test runs services under modified systemd and tests,
    whether a restart introduces a downtime into a service.
    """

    service_name = 'libzedr_systemd_test.service'
    service_file = f'/etc/systemd/system/{service_name}'

    def __init__(self, name, signal=signal.SIGHUP, **kvargs):
        super(SystemdTest, self).__init__(name, **kvargs)

        self.restart_signal = signal
    
    def test(self) -> bool:
        if not does_systemd_support_libzedr():
            self.print("Skipping test - systemd support not detected")
            return None
        if not are_we_root():
            self.print("Skipping test - not running under root")
            return None

        return super(SystemdTest, self).test()
    
    def start_server(self) -> bool:
        with open(SystemdTest.service_file, 'w') as f:
            f.write(f"""
[Unit]
Description=Libzedr test prime server

[Service]
ExecStart={self.server_name}
Restart=always
RestartTransparently=true
{f"RestartTransparentSignal={self.restart_signal}" if self.restart_signal != signal.SIGHUP else ""}
""")

        res = True
        res &= (subprocess.call("systemctl daemon-reload", shell=True) == 0)
        res &= (subprocess.call(f"systemctl start {SystemdTest.service_name}", shell=True) == 0)
        return res

    def restart_server(self) -> bool:
        return subprocess.call(f"systemctl restart {SystemdTest.service_name}", shell=True) == 0

    def stop_server(self):
        res = True
        # test that it is still running
        res &= (subprocess.call(f"systemctl show  --property=SubState {SystemdTest.service_name} | grep running", shell=True, stdout=subprocess.DEVNULL) == 0)
        # stop it
        res &= (subprocess.call(f"systemctl stop {SystemdTest.service_name}", shell=True) == 0)
        os.unlink(SystemdTest.service_file)
        return res

class SystemdMultiRestartTest(SystemdTest):
    """
    This test reuses code from SystemdTest changing only the number of restarts
    being made. It is expected that some bugs might appear due to corrupted state.
    """

    def __init__(self, name, signal=signal.SIGHUP, **kvargs):
        # 5 restarts cause a circuit-breaker trigger in systemd failing the whole service
        super().__init__(name, signal=signal, restarts=4, **kvargs)

class StandaloneMultiRestartTest(StandaloneTest):
    """
    This test reuses code from StandaloneTest changing only the number of restarts
    being made. It is expected that some bugs might appear due to corrupted state.
    """

    def __init__(self, name, signal=signal.SIGHUP, **kvargs):
        super().__init__(name, signal=signal, restarts=5, **kvargs)

class SystemdTimeoutTest(TestInterface):
    """
    This tests error states of modified systemd. Used services are expected
    to fail and this checks that that is really the case.
    """

    def __init__(self, test_name, service_command: str, timeout: int=3):
        super().__init__(test_name)

        self.service_command = service_command
        self.timeout = timeout
        self.service_name = f'systemd_timeout_test_{test_name}.service'
        self.service_file = f'/etc/systemd/system/{self.service_name}'
    
    def start_server(self) -> bool:
        with open(self.service_file, 'w') as f:
            f.write(f"""
[Unit]
Description=Libzedr integration test service with timeouts

[Service]
ExecStart={self.service_command}
Restart=always
RestartTransparently=yes
RestartTransparentTimeoutSec={self.timeout}
""")

        res = True
        res &= (subprocess.call("systemctl daemon-reload", shell=True) == 0)
        res &= (subprocess.call(f"systemctl start {self.service_name}", shell=True) == 0)
        return res
    
    def restart_server(self) -> bool:
        return subprocess.call(f"systemctl restart {self.service_name}", shell=True) == 0

    def is_service_running(self) -> bool:
        res = True
        res &= (subprocess.call(f"systemctl show  --property=SubState {self.service_name} | grep running", shell=True, stdout=subprocess.DEVNULL) == 0)
        return res

    def stop_server(self):
        res = True
        res &= (subprocess.call(f"systemctl stop {self.service_name}", shell=True) == 0)
        os.unlink(self.service_file)
        return res

    def did_failure_occur(self) -> bool:
        res = (subprocess.call(f"journalctl -u {self.service_name} --lines 10 | grep 'Failed with'", shell=True, stdout=subprocess.DEVNULL) == 0)
        return res
    
    def test(self) -> bool:
        if not does_systemd_support_libzedr():
            self.print("Skipping test - systemd support not detected")
            return None
        if not are_we_root():
            self.print("Skipping test - not running under root")
            return None

        res = True
        res &= self.start_server()
        if not res:
            self.print("Failed to start server")

        res &= self.restart_server()
        if not res:
            self.print("Failed to restart server")
        res &= self.did_failure_occur() # there must have been an error. Its an error if it wasnt there. :)
        if not res:
            self.print(f"Failure was not found in log of service {self.service_name}")

        res &= self.is_service_running()
        if not res:
            self.print("Service is not running after restart")

        res &= self.stop_server()
        if not res:
            self.print("Failed to stop server")

        self.print_success(res)
        return res


if __name__ == "__main__":
    # change dir to git root
    os.chdir(get_git_root())

    tests = [
        StandaloneTest('simple'),
        StandaloneTest('worker_threads'),
        StandaloneTest('simple_sigint', signal=signal.SIGINT),
        StandaloneTest('long_running_request'),

        StandaloneRestartOnlyTest('multiple_fds'),
        StandaloneMultiRestartTest('worker_threads'),

        SystemdTest('simple'),
        SystemdTest('worker_threads'),
        SystemdTest('simple_sigint', signal=signal.SIGINT),
        SystemdTest('long_running_request'),

        SystemdRestartOnlyTest("multiple_fds"),
        SystemdMultiRestartTest('worker_threads'),

        SystemdTimeoutTest('early_stop', '/usr/bin/sleep inf'),
        SystemdTimeoutTest('never_stops', "/bin/sh -c 'trap \\'\\' SIGHUP; sleep inf'"),
    ]

    results = []
    for test in tests:
        print(f"Running {test.name}...")
        results.append(test.test())

    print(f"\n--------------------------------\n{TERM_YELLOW}Overall results:{TERM_RESET}")
    overall_success = True

    def fw(s, w):
        sp = " "*w
        return (s+sp)[:w]

    for test, res in zip(tests, results):
        succ = f"{TERM_GREEN}SUCCESS{TERM_RESET}" if res is True else f"{TERM_RED}FAILED{TERM_RESET}" if res is False else "SKIPPED"
        print(f"\t{fw(type(test).__name__, 20)}\t{fw(test.name+ ':', 22)}  {succ}")
        if res is not None:
            overall_success &= res

    exit(int(not overall_success)) # return code logic is inverted
