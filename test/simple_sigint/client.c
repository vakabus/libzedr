#include "../integration_tests_shared.h"

#define TESTS 20

int main() {
    /* initialize random number generator deterministically */
    srand(0xdeadbeef);

    /* run tests while measurring time it took */
    for (int i = 0; i < TESTS; i++) {
        request(default_client);
    }

    test_end();
}
