#include "../integration_tests_shared.h"


int main(int argc, char** argv) {
    zedr_config_set_restart_signal(SIGINT);
    zedr_init(argc, argv);

    int socketfd = zedr_fd_init(init_server_socket, NULL);

    /* accept loop */
    while (true) {
        int fd = zedr_accept(socketfd, NULL, NULL);
        if (fd == -1) err(1, "accept");

        server_handle_request(fd);
    }

    return 0;
}
