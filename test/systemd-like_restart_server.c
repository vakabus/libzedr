#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <err.h>
#include <errno.h>
#include <stdint.h>

#include <libzedr-sm.h>

/**
 * This is a tool for manual testing of service manager integration API.
 **/
int main(int argc, char** argv) {
    if (argc != 2) {
        fprintf(stderr, "Wrong number of arguments supplied. Must be called with single argument - pid of a process to restart.\n");
        return 1;
    }

    errno = 0;
    long long_pid = strtol(argv[1], NULL, 10);
    if (errno != 0) err(1, "invalid pid");

    pid_t pid = (pid_t) long_pid;
    if ((long) pid != long_pid) errx(1, "pid out of bounds");

    int *fds;
    uint64_t *ids;

    int fd_count = zedr_sm_server_run(pid, 0, NULL, 1000, &fds, &ids);
    if (fd_count < 0) err(1, "libzedr failed to obtain the file descriptor");

    printf("Received %d fd\n", fd_count);
    for (int i = 0; i < fd_count; i++) {
        printf("FD=%d  ID=%ld\n", fds[i], ids[i]);
    }

    free(fds);
    free(ids);

    return 0;
}
