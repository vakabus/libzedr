#define _DEFAULT_SOURCE

#include <pthread.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <stdatomic.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/syscall.h> 
#include <sys/types.h>

static atomic_int count = 0;

void signal_handler(int signum) {
    count++;
}

static void install_signal_handler() {
    struct sigaction psa;
    psa.sa_handler = signal_handler;
    psa.sa_flags = 0; // don't restart interruptible syscalls
    sigemptyset(&psa.sa_mask);
    sigaction(SIGUSR1, &psa, NULL);
}

void* sleeper_thread_main(void* arg) {
    sleep(10);
    sleep(3); // because the first one will get interrupted by the signal
    exit(1); // fail when the main does not exit by itself
}

void signal_all_other_threads(int signum);

/**
 * This tests signal_all_other_threads() function from utils.c
 **/
int main() {
    install_signal_handler();

    /* start 5 threads reusing the same id because we don't care */
    pthread_t thread;
    pthread_create(&thread, NULL, sleeper_thread_main, NULL);
    pthread_create(&thread, NULL, sleeper_thread_main, NULL);
    pthread_create(&thread, NULL, sleeper_thread_main, NULL);
    pthread_create(&thread, NULL, sleeper_thread_main, NULL);
    pthread_create(&thread, NULL, sleeper_thread_main, NULL);

    /* make sure the threads started */
    sleep(1);

    /* signal them */
    signal_all_other_threads(SIGUSR1);

    /* make sure the signals had enough time to be received */
    sleep(1);

    /* test that 5 signals were received */
    printf("count=%d\n", count);
    assert(count == 5);
    return 0;
}
