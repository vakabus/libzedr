#include <assert.h>
#include <stdio.h>
#include <err.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>

#include "utils.h"

#define streq(str1, str2) (strcmp(str1, str2) == 0)

/**
 * This tests FD manipulation in util.c
 **/
int main() {
    int fds[2];
    int r;

    r = pipe(fds);
    assert(r >= 0);

    bool succ = move_fd_to(fds[0], 500);
    assert(succ);

    r = move_fd_above(fds[1], 500);
    assert(r == 501);

    char* arr[] = {"hello", "world", NULL};
    char** arr2 = strarray_dup(arr, 3);
    assert(streq(arr[1], arr2[1]));
    assert(arr2[2] == NULL);
    assert(arr2[1] != NULL);

    return 0;
}
