#include "../integration_tests_shared.h"

#include <sys/sysinfo.h>
#include <pthread.h>

#define TESTS 80
#define THREADS_MAX 8
#define THREADS (get_nprocs() > THREADS_MAX ? THREADS_MAX : get_nprocs())

void* thread_main(void* arg) {
    /* run tests while measurring time it took */
    for (int i = 0; i < TESTS / THREADS; i++) {
        request(default_client);
    }

    return NULL;
}

int main() {
    /* initialize random number generator deterministically */
    srand(0xdeadbeef);

    /* spawn threads running queries */
    pthread_t threads[THREADS_MAX];
    for (int i = 0; i < THREADS; i++) {
        pthread_create(&threads[i], NULL, thread_main, NULL);
    }

    /* wait for threads to finish */
    for (int i = 0; i < THREADS; i++)
        pthread_join(threads[i], NULL);

    /* end execution returning success or failure */
    test_end();
}
