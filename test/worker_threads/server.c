#include <semaphore.h>
#include <pthread.h>

#include "../integration_tests_shared.h"

#define THREADS 4

static sem_t thread_limit;


static void* handle_request(void* arg) {
    /* handle request */
    int fd = (int)(uintptr_t) arg;
    server_handle_request(fd);

    /* free up a thread */
    sem_post(&thread_limit);
    return NULL;
}

int main(int argc, char** argv) {
    zedr_init(argc, argv);
    sem_init(&thread_limit, false, THREADS);

    int socketfd = zedr_fd_init(init_server_socket, NULL);

    /* accept loop */
    while (true) {
        int fd = zedr_accept(socketfd, NULL, NULL);
        if (fd == -1) err(1, "accept");

        /* limit number of threads */
        sem_wait(&thread_limit);

        pthread_attr_t attrs;
        pthread_attr_init(&attrs);
        pthread_attr_setdetachstate(&attrs, PTHREAD_CREATE_DETACHED);

        pthread_t thread;
        pthread_create(&thread, &attrs, handle_request, (void*)(uintptr_t)fd);
    }

    return 0;
}
