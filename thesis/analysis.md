# Analysis

## Simplifying assumptions

For the analysis, we assume that the host system is immutable. It does not change in any way unless explicitly specified. In practice, almost no rule about the system holds every time. Nothing prevents us from loading a kernel module that changes everything. This simplification ensures that all statements about the behaviour of the system will not have to discuss all possible modifications of the system.

The second simplification we make is that the system is deterministic from a single process's perspective. For example, when a process reads `/dev/urandom`, it receives the same sequence of bytes as any other process reading this file. This assumption simplifies reasoning about the process's state.

Both assumptions are made only for this analysis. They are not required by the implementation.

## Service interruptions

Any service must be able to receive requests. Because we are talking about systems running the Linux kernel, all processes must call a syscall in order to communicate. There are exceptions to this rule, but those are generally edge-cases (e.g. kernel bypass networking) or vulnerabilities in the system (e.g. the Rowhammer attack \cite{Rowhammer}). When any program performs any I/O operation, it uses a file descriptor to refer to the communication context stored within the kernel. The program can also use the `mmap()` syscall to map some I/O context into the virtual memory and reduce the number of syscalls being made. However, reads and writes into the mapped memory block are equivalent to direct syscalls using the file descriptor used for the mapping.

The main form of communication with services are sockets. Some internal services use Unix sockets; other use networking sockets. We will call them just sockets because the differences are not relevant to our problem. The kernel manages sockets and the buffers associated with them. Any arriving request will get buffered by the kernel, and its data will not be received by the program until explicitly requested by a syscall. Buffered communication implies that programs do not have to be actively waiting for data all the time. This behaviour is not limited to sockets, it works with any I/O mechanism (e.g. services using named pipes), but sockets are the most commonly used.

There are generally two ways a service can communicate. The service can utilise a connection-based communication channel such as TCP sockets, or it can use message-based (connectionless) channel such as UDP.

When a client connects to a connection-based service, the new connection is queued within the listening socket's I/O object. The service can retrieve it by the `accept()` syscall \cite{Accept}, which returns a new file descriptor referring to the newly made connection socket. To simplify, we can assume that a single connection can be used only for a single request. It might not technically be true, but we can think of multiple requests over a single connection as one request containing several subqueries. Therefore, a connection in a queue in the kernel is semantically the same as an abstract request in a queue in the kernel.

A client connecting to a connectionless service sends a message, which is queued by the kernel. The message must contain all information for the service to fulfil the request. If that is not the case, the service can not be stateless. It would have to keep state to reconstruct the full request.

Both communication mechanisms described above can be therefore reduced to queues of abstract requests stored in the kernel. There are cases where the reduction is not possible, but then the services using the mechanisms are either stateful or not matching our requirements. Either way, those cases are complex, and we do not want to optimise for them.

If we want to provide a service with no downtime during restart, we can not stop the request handlers. That would be observable from the client's perspective. If we stopped just before fetching a new request from the request queue and started a new instance of the same program reading from the same queue, nobody would notice. Taking this even further, some request handlers might be running for a long time. If we waited until all handlers end before starting the new instance, there might be a downtime. To fix this, we could run the new instance and the old terminating instance in parallel. See Figure \ref{simpledia} for a sequence diagram.

![Sequence diagram of the transparent restart sequence \label{simpledia}](res/seq_dia_simple.pdf)

Therefore, in order to not interrupt the service, we should:

* communicate the restart (see Section \ref{scheduling-the-restart})
* start a new instance of the service handling newly arrived requests from the same queue as the old process (see Section \ref{preserving-state-of-the-service})
* stop receiving new requests while keeping already running handlers running (see Section \ref{graceful-shutdown})

## Scheduling the restart

When we want to restart a service while cooperating with it, we have to let it know about our intent. There are several IPC mechanisms available in Linux, many of which we could use, but they usually involve active participation on the receiving process's part. Active participation is a problem, because I/O syscalls could block all threads. Only the POSIX signals allow us to notify a process without it actively cooperating.

A process in Linux is the same thing as a thread group. When a thread group receives a signal, the signal handler will be called in an arbitrariry chosen thread that did not mask the particular signal \cite{Signal}. It will get called regardless of what it was doing before; even a blocking syscall would get interrupted by returning an error and setting the `errno` to `EINTR`. Due to this, we do not have to actively wait for a request to arrive. Moreover, POSIX signals are usually used to stop or reload services, and our library provides something semantically similar.

Any other IPC mechanism would require a separate thread that would not be blocked by a blocking syscall. Alternatively, we could ask the users of our library to ensure active participation themselves, but we wanted an easy to use library.

So, let us use signals as they provide the most straightforward option. Furthermore, let us use the `SIGHUP` signal as the default, because it is often used to reload daemons \cite{SOSignal}.

## Preserving state of the service

Even though we are solving transparent restarts for stateless services, there is nevertheless some state that we should pass onto the new instance of the service. An example of such a state is the current working directory. If we would start the new instance in a wrong working directory, it could behave differently and fail.

The state of a process can be divided into two parts -- the state in userspace and the state in kernel space. Kernel state can be then divided further. The following sections discuss possible solutions about how we can handle all cases separately. Last, we will conclude with what our library implements.

### Userland state

Userland state consists of the process's memory and its registers. Processes that provide stateless services have a state the same way all other processes have a state. Every process has some state even before its first instruction is executed. The in-kernel program-loader loads code from the executable into the new process's memory. It sets the instruction pointer register. This is the first userland state created. However, this state can be recreated at any time by loading the program again. Furthermore, being a stateless service means that the state does not depend on any requests but only on other immutable parts of the system. Due to immutability and determinism of the whole system, the process must be able to recreate the state every time it starts. Therefore, we do not have to consider the userspace state.

To give an example of a process with an irrelevant userspace state, let us consider a stateless service that receives a number $n$, and replies with the $n$-th prime number. In order to improve latency, the process can precalculate all reasonably large primes and store them in memory, creating a userspace state. After a restart, it does the same with the same results. So we do not have to preserve anything, the state can be recreated again. Another example might be a service that serves chunks of a large file. It might load that file into memory to speed up access time, but due to immutability of the system, we can recreate that state by rerunning the program.

### Kernel-space state -- process identification

On Linux systems, processes are identified by their process ID, PID for short. Every time a new process starts, it gets a new, unused PID.

To preserve PID, we can not call the `fork()` or `clone()` system calls to spawn a new process for the service. Nevertheless, we can call `execve()` from within the existing service process. By doing that, only the process's userspace state gets replaced by the newly loaded executable. However, not only PID is preserved, but also a much wider variety of data structures in the kernel, such as the process's owner, capabilities and more. Also, if a multithreaded process calls `execve()`, all threads get forcefully terminated, potentially interrupting requests that are being handled.

Another approach to preserve PIDs might be by misusing their recycling. PIDs are represented by continuously incrementing integers. To find the next PID, the kernel increments the last one used and tests whether it is free. If it is, that PID will be assigned to the new process. There is, however, a limit in the kernel setting the PID's upper bound. The limit is accessible by reading the `/proc/sys/kernel/pid_max` file. The root user can change the limit. When the kernel runs out of PIDs, it wraps PIDs around starting again from $1$. This behaviour can be misused to start a process with almost any desired PID. First, we need to stop the process with the PID we want. Then, we spawn many new short-lived processes and check whether one of them has received the desired PID. Until that happens, we keep spawning new.

Both described approaches have their downsides. The first approach is problematic because it preserves more than we want. Some state could have been set by the running process manually, and when we do a restart, the new process could attempt to do the same change again and fail. An example of such failure might be a process that starts with elevated privileges, opens a socket on a privileged port and then drops the privileges. Restarting it with the already lowered privileges might cause the new process to fail.

Misusing the PID recycling is not guaranteed to work. There could be a different process that gets our desired PID instead. We would have to wait until it terminates to try again and it does not have to terminate at all. Moreover, exhausting all PIDs is a slow process and stresses the system. The PID wrap-around behaviour is also not intended to be used in this manner so it might break in the future.

There is another PID-related state that the process is associated with. Processes are part of a process tree, and every process has a parent except for init with PID 1. The parent PID can be obtained from within the child process by the `getppid()` syscall. The parent receives the `SIGCHLD` signal when some of its children terminates, and it is expected that it will call `wait()` to receive the child's status and free the kernel's resources that were used by the child.

Keeping the same parent after restart is guaranteed by the first described PID preserving method -- by calling `execve()`. The other described method, using PID recycling, can be made to preserve the parent by using `clone()` with the `CLONE_PARENT` flag \cite{Clone}. However, we do not know whether the parent will call `wait()`. That is not guaranteed, and it is not under our control. So if we want to preserve the parent, the only correct option is using `execve()`.

Overall, in order to preserve PID and parent PID, we need to use the `execve()` syscall to change the executable of our process. We can not call the `fork()` or `clone()` syscalls.

### Kernel-space state -- I/O-related state

As we have noted in Section \ref{service-interruptions}, using the I/O mechanisms is connected to the usage of file descriptors in userspace. File descriptors are per-process numbers identifying a data structure somewhere in the kernel. They link to a per-process file descriptor table that in turn links to global kernel data structures. Those global data structures are reference counted, and the resource is freed after the last process closes its file descriptor. Figure \ref{fds} illustrates the file descriptors graphically.

![File descriptors example \label{fds}](res/fds.pdf)

If we want to preserve I/O, we have to preserve the content of the file descriptor tables that are associated with the process. Moreover, because the file descriptors are only opaque numbers meaning nothing on their own, it does not matter if their values change. The only important part is that they must link to the same resource as before. We just have to make sure we use them in the same context as before. That is a non-issue for us, because we limited ourselves (see \ref{requirements-for-the-programs-using-our-library}) just to one communication channel.

Some syscalls allow us to manipulate the file descriptor tables in relation to multiple processes. `fork()` copies the file descriptor tables of the calling process into the newly spawned child. `clone()` can be configured to do the same by not setting the `CLONE_IO` flag. `sendmsg()` allows us to send a file descriptor over a Unix socket \cite{UnixSocket}. It copies the I/O object reference from the file descriptor table of the sender to the receivers.

Both `fork()` and `clone()` are not selective about which file descriptors they copy. They operate on the level of the whole file descriptor tables. On the other hand, sending file descriptors over a Unix socket can be precisely controlled. We can choose which file descriptor to send and which not.

As discussed in Section \ref{service-interruptions}, preserving file descriptor tables and associated kernel structures prevents disruptions in communication. The process that receives the file descriptor can continue interacting with the outer world the same way the sender did.

To conclude, preserving file descriptors is possible on fine-grained level by sending them over Unix sockets. We did not find any downsides to it. When operating in bulk, `fork()` or `clone()` could be used too.

### Kernel-space state -- security context

The namespace, the cgroup, the process owner and the group, the root file system, etc. Those are all states that a process can have. Generally, they limit what the process can interact with. For example, a process can call the `chroot()` syscall to an empty directory to limit its view of the file system; or call `prctl()` to limit access to itself from debuggers. Some parts of the state can be changed at runtime. Other could be inherited from the parent. Most importantly, some of the runtime changes are irreversible, because they are, in essence, dropping some privilege. A privilege can not be escalated, only reduced. Therefore, we should try to preserve the state at an earliest possible point at the process startup. By doing it later, we might not be able to restore it properly.

To store the state, we could use `fork()` or `clone()` to copy it to another process (let us call it a shelter process) at an early point in the service process's execution. The shelter process would stay idle until the need for a restart arises. The new service process would have to be spawned as the child or sibling of the shelter process to inherit its state. This is, however, not without its issues. Processes drop privileges to improve security, and by preserving them, we circumvent that. Saving privileges in separate process limits the surface available for exploits, but we still could not guarantee the same level of protection as before.

There is another option to keep the process state. Another process must have started the service process,  intentionally creating some initial state. If we could reproduce that, our state could be recreated without the shelter process lingering around. A usual way to manage services is through a service manager. The service manager's job is to watch its children and to make sure they are running as configured. The service manager, by definition, must know how to start the service and can do it the same way every time. So a service manager integration would solve the security-related kernel state preservation problem for us.

Preserving security context can therefore be done by cloning it at the process's early startup by `clone()` or `fork()`. Alternatively, it can be delegated to the service manager.

### Kernel-space state -- the rest

There is still some kernel space state we have not talked about, such as the command-line arguments, the program's environment variables and the current working directory. These are all not security or IO related, but they play a crucial role in deciding the process's behaviour. This state is fully configurable at runtime, and we can simply save it at the start of the process's execution and restore it later for the restart.

### Summary of state preservation techniques

| Type of state | Method to preserve it |
|---------------|-----------------------|
| Process identification | forbid `fork()` and `clone()`, use only `execve()` |
| IO state | send FDs over Unix socket |
| Security context | early `fork()` or `clone()` or service manager integration |
| the remaining context | can be directly saved and restored from userspace |

It is apparent from the table above that keeping process identification state is not compatible with keeping security context. We cannot easily have both. Nevertheless, in practice, preserving process identification is not essential, and most processes do not change behaviour based on their PIDs.

Our library implements a two-component solution employing the service's process and _a restart server_. The restart server is a separate process assisting the transparent restart. It receives the file descriptors over a Unix socket and spawns the new instance. Any process can play the role of the restart server -- the used service manager or a newly-spawned special-purpose process.

Both components must cooperate to achieve full state restoration. We intentionally ignore the process identification state and do not attempt to preserve it, because we think that it is irrelevant to most services.

## Graceful shutdown

After we receive the restart signal and pass on the process's state, we want to terminate gracefully and stop receiving any more requests while not interrupting the already running request handlers. Some services might have already implemented a graceful shutdown, so a good option is to use that. We made that a configuration option in our library.

When the service using our library does not have an existing method of graceful shutdown, we can try to provide it ourselves. However, we can not make one procedure that will fit all services. There will always be some edge cases in which the graceful shutdown breaks. Therefore, we implement the graceful shutdown procedure on a best-effort basis. We try, but we can not guarantee the outcome.

As discussed in Section \ref{requirements-for-the-programs-using-our-library}, the program should be in an accept loop most of the time. In a multithreaded service, each thread might have its accept loop. Alternatively, there could be one master thread accepting requests and distributing them to worker threads. Either way, there is a point in the program flow where a request was handled, and a new one has not yet been accepted. If we stop the accept loop at that point, we will achieve a graceful shutdown. There are, however, some underlying issues we have to solve.

We can be blocked by a syscall waiting for a new request to come in. That is the point where we want to stop the service, but we can not run any code. The only way how to break out of the blocking call is by receiving a signal, which will interrupt the syscall. As we mentioned in Section \ref{scheduling-the-restart}, the syscall would then end with the `EINTR` error. If we did not interrupt the syscall, we would have to wait until a new request comes in and stop after that. So if we make sure that every thread receives a signal, we can provide a wrapper to syscalls in the following manner:

```
point_for_graceful_shutdown()
syscall(...)
if returned the EINTR error:
    point_for_graceful_shutdown()
```

However, the wrapper written above is prone to race conditions \cite{TheSleepingBarber} when the signal arrives just between the first point for graceful shutdown and the syscall. It is possible to solve the problem in some cases by blocking signals by `sigprocmask()`  and using `ppoll()` to wait for data or a signal. However, this race condition is not a big issue because it will eventually resolve itself after one more received request. The graceful shutdown procedure would only take more time and one additional received request by the terminating service instance.

Linux has the `tgkill()` syscall that allows us to signal a specific thread. We could use this to signal every threads, but we would have to get thread identifiers (i.e. TIDs) from somewhere atomically. The `procfs` file system exposes TIDs in `/proc/pid/task`, but reading the content of a directory is not atomic. There are possible solutions to this problem. We could stop the process with `SIGSTOP`, read the TIDs, send signals to every thread and resume the process by `SIGCONT`. However, we think that this solution introduces too much complexity and it is still not perfect (e.g. subprocess sending `SIGCONT` to its parent in an infinite loop). We did not find a way to reliably signal all threads without race conditions.All solutions can break under some edge cases. Moreover, even if a race condition occurs, the service would stop after a single new request for every thread. Therefore, our library will use the method prone to race conditions. We consider it good enough. The service itself should handle graceful shutdown by itself to fix the issue properly.

Another problem might be that the accept loop uses async I/O and mixes existing requests handlers with new connection handlers. In that case, we can not stop the loop. It might look like the following example:

```
events = [...]  # an array of pending events, one of which is
                # a new connection handler
while True:     # async I/O event loop
    i = poll(events, ...)  # return an index of newly active event
    events[i].advance_handler()  # run the event's handler
```

To stop accepting new requests in the example above, it is necessary to remove the new request handler from the event loop. That is very much implementation-specific. We could provide an implementation for common async I/O libraries. However, as with the signalling race conditions, implementing a solution is too complicated, and the result is, in our opinion, not worth it.

Given all circumstances discussed above, all edge cases can be handled by the custom graceful shutdown configuration option. The most common cases can make use of a graceful shutdown implemented by our library.

## The restart procedure

A user sends a restart signal to our service. The signal handler is run in a random thread setting a restart flag. It then returns, and the service runs again normally until the first safe point for shutdown is reached. Due to the restart flag being set, the procedure in the safe point saves the necessary state and spawns the restart server. The service then sends the saved state to the restart server over a Unix socket.

After receiving the state, the restart server spawns a new instance of the service, while restoring the state it received. It then stops, because the restart server is no longer necessary.

After sending the state, the old service tests configuration, whether a custom graceful shutdown procedure was set. In it was not, the restart procedure sends the same restart signal again but this time to all threads one by one. Then the thread exits. The signal is received by the other threads, the signal handlers sets the same flag over again with no effect. However, blocking syscalls are interrupted, and the threads flow into a safe point for termination too. They determine that the restart was already handled by other thread and they exit. So eventually, the whole process stops.

If the custom restart procedure was set, the first running restart procedure calls the provided function and normally returns afterwards. The service is then expected to handle graceful shutdown by itself.

The diagram in Figure \ref{complexdia} illustrates the procedure graphically.

![Restart procedure diagram\label{complexdia}](res/cropped_restart_procedure_diagram.pdf)

The restart procedure can be either standalone -- the restart server is spawned as a single purpose subprocess. We described this approach above. Alternatively, the restart can be assisted from the beginning by the service manager, which would start the restart server instead of the service's process itself.

## Conclusion

We have discussed all problems that our library needs to solve. Our solution has, therefore, these features:

* We use POSIX signals to initiate the restart.
* We can preserve all state except for process identification while cooperating with the service manager.
* We can preserve only I/O and freely modifiable state when not cooperating with the service manager.
* The provided graceful shutdown functionality works on best-effort basis. Alternatively, the service can implement its own graceful shutdown procedure.
* The new instance of the service runs in parallel to the old terminating instance.
* We use the restart server to manage the restart procedure. The restart server is either integrated into a service manager or can be spawned ad hoc.
