# Attachments


| File name | Description |
|---------------|-----------------------|
| `systemd.patch` | git patch with our systemd modifications; built on top of commit `2d4f8cf467b6` in the upstream systemd git repository available at \url{https://github.com/systemd/systemd} |
| rmbt-server.patch | git patch with our RMBT server modifications; built on top of commit `a69463c1d900` in the upstream rmbt-server git repository available at \url{https://github.com/rtr-nettest/rmbt-server} |
| libzedr/ | source code of the libzedr library excluding the code for building the thesis's PDF |
| prepare_env.sh | shell script that clones the upstream repositories and applies the patches |
| README.md | more detailed description of the attachments and a guide explaining usage |


The same code can be found in these hosted git repositories:

* \url{https://gitlab.com/vakabus/libzedr} -- the libzedr library
* \url{https://gitlab.com/vakabus/libzedr_systemd} -- fork of systemd with libzedr integration
* \url{https://gitlab.com/vakabus/libzedr_rmbt-server} -- rmbt-server fork with libzedr-provided transparent restarts

