# Technical design decisions

## Architecture of the solution

There are several options on how to design the library from a high-level perspective. One option is a **standalone design** in which we make a single library with no external dependencies. The design has the smallest possible API, because we would be interfacing only with the service's code. The restart server would be an internal component of the library. This design disallows deep integration into a service manager. On the other hand, it would be service manager agnostic.

Another option would be to use multiple public components. A **library** for the service and **an external restart server**, which could be integrated into a service manager. Each component would have an API, and even the interactions between them would need to be considered public API due to possible version mismatches. This will disallow future incompatible changes. However, the integration into the service manager would be straightforward.

We would like to have both described features. The smallest possible API to make the library simple to use. A good integration into service managers is also a requirement because it would allow the preservation of the security context (see \ref{kernel-space-state-security-context}). Therefore, we can make a **hybrid library** that both the services and the service managers could use. It would have two public-facing APIs, one for either client. Interaction between those two APIs would, however, stay internal and could be changed without breaking backwards compatibility. The design would also allow for standalone restarts with reduced functionality.

Therefore we think that the hybrid approach is the best of the mentioned possibilities. Our final solution uses this design.

## Choice of a service manager

We will implement the integration into systemd. Systemd is arguably the most common Linux service manager, and we have some prior experience with it. It also already contains features that we can make use of. For example, socket activated services \cite{SocketActivationBlog1}\cite{SocketActivationBlog2},\cite{SocketActivationSystemd} -- services that get started based on activity on a configured communication channel. When socket activation is used, systemd creates the listening file descriptor, giving it to the service when it starts. We could make use of the file descriptor passing method, because that is exactly what we need.

However, using systemd does not mean that we will limit ourselves to systemd. The library will provide a simple API that any service manager can use.

## Choice of a programming language

The used language should be able to call syscalls with minimal abstraction. High-level languages like Python would be cumbersome. Also, because systemd was chosen as the service manager to which we will try to implement support, the library should have a high interoperability with C. That has left us with Rust and C itself. Those are languages in which we feel confident enough to write something like this. We then chose C, because Rust safety guarantees are not that beneficial in this context (the problem is neither in memory management nor managed multithreading) and a lot more people around systemd know C than Rust.

## Name of our library

Not a technical decision, but due to no namespaces in C, we must have the library name in the names of our exposed API calls. Otherwise a name collision could occur easily.

We have decided to name our library **libzedr** as a shortcut for **lib**rary for **ze**ro **d**owntime **r**estarts.

## Sketch of user API

Based on the observations made in Section \ref{analysis}, we propose this API:

1. `zedr_init()` -- A function that is called very early on in the service, ideally as the first thing done in step one of our model service. It saves most of the state required for a restart, and it initialises the internal state of our library. The function detects if the service runs after transparent restart and in that case, it retrieves the file descriptors.
2. `zedr_fd_init()` -- A function that registers a file descriptor. It receives a description how to initialise the communication channel in the form of a function pointer. It initialises the file descriptor, or it returns a file descriptor obtained in the `zedr_init()` function.
3. `zedr_restart_point()` -- A function that marks a place where it is safe to restart when asked. It initiates the state transfer to a new service instance. It provides graceful shutdown when present in the program flow of every non-terminating thread.

## Sketch of service manager API

For the service manager, only one function is required. `zedr_sm_server_run()` runs the restart server. It opens a Unix socket and retrieves the file descriptors from the terminating service. The file descriptors are returned to the service manager after freeing the used resources (e.g. closing the Unix socket). The service manager should then start the new service instance, passing on the file descriptors.

We also expose the underlying Unix socket initialisation and processing under functions `zedr_sm_server_init()` and `zedr_sm_server_process()`. Those can be used by asynchronously designed single-threaded service managers such as systemd. Because these calls do not block, the single-threaded service manager can perform other tasks while waiting for the service to send its file descriptors.

The start of the service is a job that our library can not handle, because that is very much implementation dependent on the service manager's internals. Any additional API would not help.

## The file descriptor passing protocol

The protocol to a pass file descriptor between the restart server and the old process is not essential to discuss here as it is an internal part of the library itself. However, passing the file descriptor from the restart server to the new service is a public API.

The file descriptor table contains the necessary references at the start of the process. The protocol must therefore solve how to let the service know about them. We use environment variables to pass this information. We use the same protocol \cite{SdListenFDs} as systemd's socket activated services. 

`$LISTEN_PID` contains the PID of the running process. This ensures that children of the service do not use the provided information as it is not meant for them. `$LISTEN_FDS` contains the number of the file descriptors provided. The first file descriptor has number $3$. The remaining file descriptors follow at $4$, $5$,... `$LISTEN_FDNAMES` contains a colon-separated list of names. Systemd considers these names optional, we require them.

Systemd provides helper methods (e.g. `sd_listen_fds()` \cite{SdListenFDs}) to read these environment variables in the libsystemd library. However, we did not want to have any dependencies on a specific service manager, so we implement a simple parser ourselves.
