# Evaluation of our solution

## RMBT server using libzedr

We used the libzedr library in a real-world service, the RMBT Test Server. Original source code of the server is available online at \url{https://github.com/rtr-nettest/rmbt-server}.

RMBT Test Server is a server developed under the Austrian Regulatory Authority for Broadcasting and Telecommunications (RTR) for measuring network speeds over the Internet. RMBT stands for RTR Multithreaded Broadband Test \cite{RMBT}. The server is a stateless service.

We had to change more code than just the minimum required 3 lines. However, the change is only a reorganisation of an existing code, we have not added anything new. We think that the necessary code change is adequate and that we have met all our goals with the libzedr library.

## Future work on libzedr

The libzedr library is, in our opinion, a feature-complete project. There are still things that could be improved, but they are not very significant. The biggest improvements could be now made by actually using the library in production, analysing its usage and fixing flaws found that way. Therefore, configuration options could be added like a custom method of restart signalling or manual graceful shutdown initiation.

One significant change could be adding generational tracking to libzedr. If we restart the service multiple times, multiple instances of the service might be running, all except for one being in the process of a graceful shutdown. However, if the service gets stuck during graceful shutdown, it might never terminate. However, this would make much more sense to implement on the service manager's side.

Aside from adding features, we have not assessed the performance implications of using libzedr. The library is not heavily optimised for performance. We do not know, what is the performance impact of having the `zedr_restart_point()` function called roughly once for every request. We do not know the effect of a transparent restart on throughput. We expect that if the service is handling requests at full capacity, performing the transparent restart might lead to some requests being dropped. Measuring, improving and documenting libzedr's performance  could help potential users decide whether to use the library or not.

## Future work on transparent restarts

A new kernel mechanism could solve some issues we have discussed in Section \ref{analysis}. If we could divert arriving requests from an old file descriptor into a new one, we could solve problems with async I/O and some race conditions. After spawning a new instance of the service, a transparent restarts library such as libzedr could setup the diversion. The old instance would no longer receive any requests, so we would not have to prevent it manually. This would allow for an even less intrusive implementation and would work even with async I/O event loops.

Another potential improvement is the new `pidfd_getfd()` syscall \cite{PidFDGetFD}, first available in Linux 5.6. This syscall allows us to duplicate a file descriptor from a running process without its cooperation. That would alleviate the need for sending the sockets over the Unix socket.

In our work, we have chosen a cooperative approach to transparent restarts. However in some cases, it is possible to provide the same functionality to non-cooperating processes. We could use ptrace to attach to the running process and modify its code at runtime. Or we could modify the dynamic linker to wrap important syscalls like `socket()` and `accept()` with wrappers similar to what libzedr provides. Our goals lead to the development of libzedr, mainly the goal about simplicity. Other approaches are indeed possible, they have however different properties.

## Future work on systemd

Systemd's integration of transparent restarts could be made much better. First, a change that might look like an improvement from systemd's perspective would be to get rid of libzedr. Second, the already mentioned generational tracking would help administrators monitor the state of the restarts.

### Transparent restarts with libsystemd

The functionality of the libzedr library could be integrated into libsystemd \cite{SdDaemon}; a library intended to make interactions with systemd easier. The main benefit of such integration from systemd's perspective is reducing the number of dependencies. Moreover, all functionality needed for systemd assisted transparent restarts is already in place within libsystemd. It is possible to send file descriptors over to the Service unit's `FDStore` via the libsystemd's `sd_notify_with_fds()` function. The `sd_listen_fds()` function handles file descriptor retrieval with an already-implemented and tested protocol.

The functionality that might be dropped by such integration are standalone restarts (see \ref{the-restart-procedure}) and support for other service managers. These features could get removed with the argument that the systemd's library should do only things related to systemd. Therefore unrelated standalone mode of operation and support for different service managers would be irrelevant to the library's goals.

There were already at least two attempts \cite{SystemdZeroDowntime} \cite{SystemdZeroDowntime2} to open a discussion about transparent restarts in systemd. However, we do not know about any progress being made.

### Generational tracking

We could say that old generations (old service processes) are still the property of the same Service unit. We could start by moving the existing processes in the Service's cgroup into a subgroup representing the old generation and continuing with the same restart procedure as we developed. We could then list child cgroups to traverse all running generations. Also, when stopping the Service's cgroup, we would stop all its children. So stop would behave just the way it should -- stopping everything related to the Service. This would require the development of new tools to interact with the generations, and it would break an invariant within systemd that Service units correspond to cgroup tree leaves \cite{SystemdCgroups}. Moreover, the cgroups v2 API does not support processes within internal tree nodes. Processes can reside only in leaves.

Another, arguably better approach would move the old processes during the transparent restart into a dynamically created Service unit. The stop job on the original Service would no longer stop all the generations; only the most recent one. Nevertheless, we could make a Slice unit for all generations. Slice unit within systemd corresponds to a cgroup tree internal node. Stopping the Slice unit would stop all generations. Moreover, a similar technique is used with the templated services (services having `@` in their name) \cite{ManSystemdUnit}, so that some code might be reused. No additional tools for interacting with the generations would have to be developed. Users not caring about generations could ignore them as the standard Service unit would be used for the most recent generation.
