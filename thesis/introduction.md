# Introduction

All businesses around the world selling their products online depend on uptime of their servers. According to Gartner \cite{GartnerDowntime} in 2014, the average cost of downtime was $5,600 per minute. Keeping servers running at all times saves money. However, assuring high-availability of provided services is expensive — for example, Czech domain registrar CZ.NIC uses \cite{CZNICInfra} three different data centres with independent connectivity to assure that their domain registration system is always online.

For smaller projects with fewer resources, the services are usually provided by a single machine. Keeping services available all the time is then impossible due to the inevitable hardware maintenance. However, some downtime still comes from software maintenance. To update a running program, a new executable must be copied in, and the program must be restarted, which introduces a downtime for the provided service.

We created a library that the developers can use to implement zero-downtime restarts (transparent restarts) in their projects. We limit ourselves to the environment of GNU/Linux systems. We fully support only stateless services as it massively simplifies the problem. That does not mean that stateful services can not utilise our library. However, they would have to implement state preservation by themselves.

## Context

The terms used in this thesis are all related to Linux-based systems. At the time of writing, the oldest supported kernel by upstream developers is version 4.4, and the most recent is 5.6. The current glibc library is version 2.31. When a term is not defined explicitly, the meaning is understood in relation to the supported kernels and systems built on top of them with the current glibc library. Example of such systems are major GNU/Linux distributions such as Debian, Fedora, openSUSE or Arch Linux.

## Notation

We use `name()` to identify a function. The brackets do not signify that the function does not take any arguments. `$NAME` refers to an environment variable. `name` without the brackets or the dollar sign is an identifier of a non-executable generic object such as a variable, a file system or a data structure.

## Glossary

By _the system_, we mean the collection of all software running on a computer. Mainly focusing on, but not limited to, the kernel, the init system (PID 1), the file system with `procfs` in `/proc`, `sysfs` in `/sys` and all other services related to keeping the system running.

_A process_ is an instance of a running program, a loaded executable. It has some internal state (e.g. data structures in memory) and a state kept by the Linux kernel itself (e.g. the current working directory). A process starts after `clone()` \cite{Clone} or `fork()`. It ends by calling `exit()`. The `execve()` syscall \cite{Execve} loads a new code from an executable into the current process.

_A process_ providing _a service_ means that the process replies to some external requests and provides appropriate responses (not necessary to the requestor). _Downtime_ in the context of _a service_ is state when the user of the service does not receive expected responses to their requests. _Without downtime_ means the same as _without interruption_.

_Process state_ or _process context_ is defined by the content of the process's virtual memory, registers and related kernel data structures.

_Transparent restart_ of a service-providing process is a restart without apparent interruption in the provided service.

_A stateless service_ is a service that does not require keeping any state in between multiple requests within the context of the process providing the service. Using external data storage (e.g. database) is allowed, because the state is not kept within the service's process.

_I/O_ means input and output. It is used as a shortcut when talking about facilities providing input and output to a process.

_A file descriptor_ is an integer used in userspace to identify an entry in the process's file descriptor table. It is used in syscall arguments to refer to the entries. File descriptors have similar semantics to pointers. Duplicating a file descriptor is the same as copying a pointer. File descriptors can be cloned using the `dup()` and `fcntl()` system calls or they can be sent over a Unix socket to another process. These operations clone just the entry in the file descriptor table, not the associated kernel object. \cite{FDTableSO}

_The file descriptor table_ is a per-process in-kernel data structure holding references to the kernel objects (e.g. I/O objects, timers, processes, etc.). Those objects are usually reference-counted so that multiple entries in file descriptor tables can point to a single object. For example, when a process wants to print something to standard output, it calls the `write()` syscall with the proper file descriptor. The kernel looks through the process's file descriptor table to find the related I/O object on which it performs the write operation.

_An I/O object_ is a kernel data structure describing any I/O mechanism such as a socket, a file or a pipe.

_A communication channel_ is mainly a Unix socket, a network socket, a pipe or generally any communication mechanism using a file descriptor and the `read()` or `write()` syscalls.

## Goals of our library

These are the high-level features we expect our library to have:

* The library _must work on Linux systems_.

* Programs using our library _must be able to perform restarts transparently_.

* Our library _should be straightforward to use_. It should not require significant changes in existing projects or in the way we design services. In our opinion, it is better to have a solution for the most common cases, not for all cases. Correctly handling everything might be possible, but it could introduce a lot of complexity unnecessary for the typical case. Our goal is not to make an all edge-case encompassing solution that is hard to use correctly. That would not be any improvement for developers over implementing the transparent restart themselves.

* Furthermore, the library _should be simple in principle_, and it should not rely on sophisticated hacks prone to breaking in the future. Every operation should be explicit, and nothing should be done without an apparent cause. This makes it easier for potential users to reason about the library usage. Allowing everybody to understand what is going on under the hood should make debugging of any potential problems more accessible to users.

## Requirements for the programs using our library

The programs using our library are expected to fulfil these features:

* The program using our library _must follow these steps during runtime_:
  1. initialise the internal state
  2. initialisation of one listening communication channel (e.g. socket by calling `socket()`, `bind()` and `listen()`)
  3. _accept loop_ -- an infinite loop accepting requests (e.g. by `accept()`) and dispatching them onto some handlers

* The program _can be multithreaded_.

* The program _can utilise subprocesses only if a restart of the parent process restarts the whole service_. This disallows service launchers that will spawn several independent processes providing the same service. In such a case, the actual workers, rather than the parent process playing a role of a service manager, follow the model described above. We argue about them individually without the context of the whole group.

* The service _uses only one communication channel to receive requests_. This limitation should not be hard to lift in the future, but it simplifies initial development and should roughly reflect the typical case.

## Example service

The following snippet of C code is a possible skeleton of a service that matches our requirements.

```
#include ...

typedef void (*handler_f)(int);

void dispatch_thread(handler_f func, int arg);

void handle_request(int fd);

int main() {
    /* internal state initialization */
    /* empty */

    /* communication channel initialization */
    int socketfd = socket(AF_INET, SOCK_STREAM, 0);

    struct sockaddr_in addr;
    /* fill in the struct */
    bind(socketfd, (struct sockaddr *)&addr, sizeof(addr));
    listen(socketfd, SOMAXCONN);

    printf("Accepting connections\n");

    /* multithreaded accept loop */
    while (true) {
        int fd = accept(socketfd, NULL, NULL);
        dispatch_thread(handle_request, fd);
    }
}
```

## Related work

### nginx

Nginx \cite{nginx} is a popular web server that supports transparent restarts \cite{NginxUpgradeDocs} in almost the same way as our library. The main difference between their approach and our approach is that they are handling multiple processes at once.

Nginx has a master process controlling the worker processes. The concept is similar to a service manager and smaller services. The master process runs under root while the worker processes run under lowered privilege. During a restart, the master process of the old instance spawns a new master process, which in turn spawns its worker processes.

### libpulp

The libpulp \cite{libpulp} library written in SUSE Labs tries to keep services running even during updates by utilising live-patching \cite{LibpulpTalk}. The actual code of the service is not be updated by libpulp, however, when a linked library such as libssl receives a new version, libpulps's tools can update all processes using the library by rewriting entrypoints into the dynamically linked library.

### systemd

Systemd has a capability to serialise its state and re-execute itself. It does not have to worry about preserving the security context, because its privileges are never dropped. Systemd is single-threaded, it can restart at any time without worrying about request handlers running in parallel. As a init system, systemd must not change its PID, so it uses only the `execve()` syscall to restart.

### CRIU

CRIU \cite{CRIU} stands for Checkpoint/Restore In Userspace. CRIU is a tool trying to solve the state preservation problem for a generic process. It can serialise the state of any process at any point and restore it later. CRIU requires support from the kernel; therefore it works best with recent kernel versions containing all their patches \cite{CRIUPatches}. The main use case is the live-migration of processes between physical machines. It is used for example at Google \cite{CRIUatGoogle} for their infrastructure.


