\chapter{The problem}

All businesses around the world selling their products online depend on uptime of their servers. \href{https://blogs.gartner.com/andrew-lerner/2014/07/16/the-cost-of-downtime/}{According to Gartner} in 2014, the average cost of downtime was \$5,600 per minute. Keeping servers running at all times saves money. However, assuring high-availability of provided services is expensive — for example, Czech domain registrar \href{https://www.nic.cz/page/350/registry-system/}{CZ.NIC uses} three different data centres with independent connectivity to assure their domain registration system is always online.

For smaller projects with fewer resources, the services are usually provided by a single machine. Keeping services available all the time is then impossible due to inevitable hardware maintenance. However, some downtime still comes from software maintenance. To update a running program, a new executable must be copied in, and the program must be restarted, which introduces a downtime for the provided service.

We created a library that developers could use to implement zero-downtime restarts (transparent restarts) into their projects. We limit ourselves to the environment of GNU/Linux systems. We will fully support only stateless services as it massively simplifies the problem. That does not mean that stateful services can not utilise our library. However, they would have to implement state preservation by themselves.

\section{Context}

Terms used in this thesis are all related to Linux-based systems. At the time of writing, the oldest supported kernel by upstream developers is version 4.4, and the most recent is 5.6. The current glibc library is version 2.31. When a term is not defined explicitly, the meaning is understood in relation to the supported kernels and systems built on top of them with the current glibc library. Example of such systems are major GNU/Linux distributions such as Debian, Fedora, openSUSE or Arch Linux.

\section{Notation}

We use {\tt name()} to identify a function. The brackets do not signify, that the function does not take any arguments. {\tt \$NAME} refers to an environment variable. Without the brackets or the dollar sign, {\tt name} is an identifier of non-executable generic object such as a variable, a file system or a data structure.

\section{Glossary}

By \emph{the system}, we mean the collection of all software running on a computer. Mainly focusing on, but not limited to, the kernel, the init system (PID 1), the file system with {\tt procfs} in {\tt /proc}, {\tt sysfs} in {\tt /sys} and all other services related to keeping the system running.

\emph{A process} is an instance of a running program, a loaded executable. It has some internal state (e.g. data structures in memory) and a state kept by the Linux kernel itself (e.g. current working directory). A process starts after a {\tt clone()} or {\tt fork()}. It ends by calling {\tt exit()}. The {\tt execve()} syscall loads a new code from an executable into the same process as before.

\emph{A process} providing \emph{a service} means that the process replies to some external requests and provides appropriate responses (not necessary to the requestor). \emph{Downtime} in the context of \emph{a service} is a state when the user of the service does not receive expected responses to their requests. \emph{Without downtime} means the same as \emph{without interruption}.

\emph{Process state} or \emph{process context} is defined by the content of the process's virtual memory, registers and related kernel data structures (e.g. {\tt struct task\_struct}).

\emph{Transparent restart} of a service providing process is a restart without apparent interruption in the provided service.

\emph{A stateless service} is a service that does not require keeping any state in between multiple requests within the context of the process providing the service. Using an external data storage (e.g. database) is allowed, because the state is not kept within the service's process.

\emph{I/O} means input and output. It is used as a shortcut when talking about facilities providing input and output to a process.

\emph{A file descriptor} is an integer used in userspace to identify an entry in the process's file descriptor table. It is used in syscalls to manipulate the entries. File descriptors can be cloned using {\tt dup()} and {\tt fcntl()} system calls. This operation clones just the entry in the file descriptor table, not the associated kernel object. We can think about file descriptors the same as about pointers. Duplicating them is the same as copying a pointer. File descriptors can be copied to other processes by sending them over a Unix socket.

\emph{The file descriptor table} is a per-process in-kernel data structure holding references to the kernel objects (e.g. I/O objects, timers, processes, etc.). Those objects are usually reference-counted so that multiple entries in file descriptor tables can point to a single object. For example, when a process wants to print something to standard output, it calls the {\tt write()} syscall with the proper file descriptor. The kernel looks through the process's file descriptor table to find the related I/O object on which it performs the write operation.

\emph{An I/O object} is a kernel data structure describing any I/O mechanism such as a socket, a file or a pipe.

\emph{A communication channel} is mainly a Unix socket, a network socket, a pipe or generally any communication mechanism using a file descriptor and the {\tt read()} or {\tt write()} syscalls.

\section{Goals of our library}

These are the high-level features we expect our library to have:

\begin{itemize}
\item The library \emph{must work on Linux systems}.
    
\item Programs using our library \emph{must be able to perform restarts transparently}.

\item Our library \emph{should be straightforward to use}. It should not require significant changes in existing projects or in the way we design services. In our opinion, it is better to have a solution for the most common cases, not for all cases. Correctly handling everything might be possible, but it could introduce a lot of complexity unnecessary for the typical case. Our goal is not to make an all edge-case encompassing solution that is hard to use correctly. That would not be any improvement for developers over implementing the transparent restart themselves.

\item Furthermore, the library \emph{should be simple in principle}, and it should not rely on sophisticated hacks prone to breaking in the future. Every operation should be explicit, and nothing should be done without an apparent cause. This makes it easier for potential users to reason about the library usage. Allowing everybody to understand what is going on under the hood should make debugging approachable to users.
\end{itemize}

\section{Requirements for the programs using our library}

The programs using our library are expected to fulfil these features:

\begin{itemize}
\item The program using our library \emph{must follow these steps during runtime}:

\begin{enumerate}
    \item initialise internal state
    \item initialization of one listening communication channel (e.g. socket by calling {\tt socket()}, {\tt bind()} and {\tt listen()})
    \item \emph{accept loop} -- an infinite loop accepting requests (e.g. by {\tt accept()}) and dispatching them onto some handlers
\end{enumerate}

\item The program \emph{can be multithreaded}.

\item The program \emph{can utilise subprocesses only if a restart of the parent process restarts the whole service}. This disallows service launchers that will spawn several independent processes providing the same service. In such a case, the actual workers follow the model described above, not the parent process playing a role of a service manager. We argue about them individually without the context of the whole group.

\item The service \emph{uses only one communication channel to receive requests}. This limitation should not be hard to lift in the future, but it simplifies initial development and should roughly reflect the typical case.

\end{itemize}

\section{Example of the model service}

The following snippet of C code is a possible skeleton of a service, that matches our requirements.

\begin{code}[frame=none]
#include <stdio.h>
#include <sys/socket.h>
#include ...

typedef void (*handler}f)(int);

void dispatch}thread(handler}f func, int arg);

void handle}request(int fd);

int main() {
    /* internal state initialization */
    /* empty */

    /* communication channel initialization */
    int socketfd = socket(AF}INET, SOCK}STREAM, 0);
    listen(socketfd, SOMAXCONN);

    struct sockaddr}in addr;
    /* fill in the struct */
    bind(socketfd, (struct sockaddr *)&addr, sizeof(addr));

    printf("Accepting connections\n");

    /* multithreaded accept loop */
    while (true) {
        int fd = accept(socketfd, NULL, NULL);
        dispatch}thread(handle}request, fd);
    }
}
\end{code}

