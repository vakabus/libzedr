# The libzedr library

The implementation we have made closely matches what we have discussed in Section \ref{analysis}. The code can be found in the attachment or the GitLab repository at \url{https://gitlab.com/vakabus/libzedr}.

## Library initialization (`zedr_init()`)

As we discussed in Section \ref{kernel-space-state-the-rest}, some kernel state needs to be explicitly saved. Most importantly, the command-line arguments, the current working directory (CWD) and the environment variables (ENV). CWD and ENV are easy to get from anywhere in the process, but obtaining the command-line arguments is hard. The easiest thing to do then is to ask the user for them. So the initialisation function takes pointers to the arguments array.

There are ways to get the argument array without asking for it. We could work with the dynamic linker and put a function pointer into the `.init_array` section. The function would get called before `main()`, and it would be given the same arguments as `main()`. If we look into the glibc source code, into the file `csu/libc-start.c` in the commit `109474122400ca7`, we can see the glibc initialisation function calling the `init()` function on line 264 \cite{GlibcStart}. The `init()` function is provided as an argument by a platform-dependent code written in assembler in the `sysdeps/$ARCH/start.S` file. There, we can find that the `init()` function is actually the `__libc_csu_init()` in the `csu/elf-init.c` file. At the end of it, the `.init_array` functions are called on line 88:

```
86|  const size_t size = __init_array_end - __init_array_start;
87|  for (size_t i = 0; i < size; i++)
88|      (*__init_array_start [i]) (argc, argv, envp);
```

However, getting the arguments using the method described above is not easily understandable. It does not follow our requirements that the library should be simple in principle. The same argument can be used for reading the array from within `/proc/PID/cmdline`. We would still have to do it very early, because the program can change the content of this file by changing the argument array (for example `getopt()` does that). Asking for those arguments in the library initialisation function is the most straightforward thing we can do.

We should also consider that we want to make some features of the library configurable (e.g. the signal used for signalling a restart). However, if we want to allow users to use our library without adding or changing too much code, the configuration should not be required by default. One option how to do that would be to use variadic `zedr_init()` function with optional configuration as the last argument. Nevertheless, that is not type-safe, and the user is likely to get the arguments wrong. It also does not play nice with IDEs. A more straightforward solution is adding configuration functions that can be called before the `zedr_init()` function. They can be named semantically, so that the user knows what they are changing. We used this method; for example the signal can be changed using the `zedr_config_set_restart_signal()` function.

The `zedr_init()` call and the configuration functions are single-threaded, because it makes sense to call them only once in the whole service. The functions checks it to limit the possibility of accidental errors.

## File descriptor registration (`zedr_fd_init()`)

The function for file descriptor registration should be able to internally decide whether to register or reuse a passed-in file descriptor. That can be achieved by having the function take a function pointer to the communication channel initialisation code (e.g. function that creates a socket and returns its file descriptor). That function will be called lazily, only if the process is running for the first time. User data could be optionally passed through to it as a void pointer. The process is illustrated by the following pseudo-Python code snippet.

```
def zedr_fd_init(create_fd, arg):    # create_fd is a function
    if runnning after transparent restart:
        return the previously used file descriptor
    else:
        return create_fd(arg)
```

The same functionality can be provided by a function returning the previously used file descriptor or an empty value such as a negative number. However, this would usually require a much bigger change in existing code bases due to the newly required logic. By taking a function pointer, we can hide the logic within the library. We do not think that this is an unnecessary complexity, because it simplifies reasoning about the API call. Semantically, it always does the same thing -- it initialises the communication channel.

There are some downsides to this approach. There is no way to detect that the service configuration has changed and that the communication channel should be reinitialised. However, we can safely ignore this issue. First, it is a rare event. Changing the listening ports is not as common as restarts due to updates. Second and more importantly, the changing configuration like this is in itself not an interruption-free change.

This API call is also inherently single-threaded. It gets called once for every used listening file descriptor.

## Restart point (`zedr_restart_point()`)

The `zedr_restart_point()` is the function where the restart can occur. The service can be scheduled for restart since calling `zedr_init()` function, which installs the signal handler. Receiving the signal right after that causes the restart flag to be set to true, and therefore even the first restart point reached can cause service restart, possibly without handling any single request.

The inner workings of this API call have been discussed in Sections \ref{graceful-shutdown} and \ref{the-restart-procedure}. Our implementation does the same as described there.

The API call must be able to handle concurrent use. Services wanting to utilise the full performance of modern multi-core CPUs must use some form of parallelism and as we discussed in the paragraph above, restart points might be present in every running thread.

We need to prevent the restart procedure from running multiple times. We lock a mutex before starting the restart procedure, and we never unlock it again. If a thread sees the mutex locked, it does not start the procedure. The same effect could have been achieved by compare-and-swap instructions and a boolean flag.

## Tests

To help with the development, we created a simple test service providing prime numbers over a TCP socket. A request consists of a number $n$ and the response is the $n$-th prime. To find the $n$-th prime, the service intentionally uses a naive slow method to test that the graceful shutdown works correctly.

The test service is implemented several times, every time with some differences. We use two different design paradigms. We have a single-threaded service and a thread pool dispatching service.

All test cases are managed by a test script written in Python. It runs the services under systemd and without systemd. The script starts a server, then a client sending requests to the server. After a few requests, the script restarts the server.

The client sends signals to its parent, `SIGUSR1` or `SIGUSR2` for every request. `SIGUSR1` in case of a successful request, `SIGUSR2` in case of a failed request. Using this method, the script monitors the requests. If any request fails, the test case is considered failed.

# Documentation of libzedr

The documentation of libzedr is included within the library's source code. It starts with the `README.md` file in the top-level directory, and it branches further into details. To make reading easier, interactive HTML documentation can be generated using Doxygen. The generated documentation is attached to this thesis and the generation procedure is described in the `README.md` file.

We have included the documentation in the source code because it is easily accessible to potential users. The documentation format is the also de facto standard in open-source software and platforms like GitHub and GitLab display the content of the `README.md` file by default when viewing the repository.

For convenience, the latest automatically built documentation might be accessed online at \url{https://gitlab.com/vakabus/libzedr/-/jobs/artifacts/master/file/docs/html/index.html?job=docs}.
