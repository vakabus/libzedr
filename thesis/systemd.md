# Integration of libzedr into systemd

## Systemd's data model

On the highest level, systemd works with objects called Unit \cite{SystemdUnit}. Basic object-oriented programming techniques are employed \cite{SystemdUnitVTable}, and the Unit is, in a sense, an abstract class. It has several more specific implementations (subclasses), one of which is the Service object.

Systemd uses cgroups \cite{ManCgroups} to track processes. Units usually have some relation to cgroups \cite{SystemdCgroups}. Service unit corresponds to a leaf cgroup in the cgroup tree. Every process that is part of the Service's cgroup is considered a property of the Service and is therefore managed by it. All processes within the cgroup are killed when the service is stopped.

The Service \cite{SystemdService}, as all other Units, is controlled by a simple finite-state automaton. The states \cite{SystemdServiceState} correspond to the state of the running program; for example, states `SERVICE_RUNNING` and `SERVICE_DEAD` indicate whether the service is running or not. Other automaton states such as `SERVICE_START_PRE`, `SERVICE_START` and `SERVICE_START_POST` correspond to transient service states during the Service unit start procedure. Usually, a state represents a situation where systemd is waiting in an event loop for a new event. The `SERVICE_RUNNING` state transitions into another when the service stops by itself (i.e. waiting for the `SIGCHLD` signal) or when a user requests a change. This allows systemd to process multiple operations at once asynchronously.

A request to change the state of a Unit is defined as a job. Jobs are queued, and their execution is ordered based on the ordering dependencies of their corresponding Units. For example, if we want to start a graphical login manager, systemd has to wait and launch it after the X11 server is running. The dependency would be configured within the Service configuration of the X11 server or the graphical login manager. Independent jobs can be processed at the same time.

Systemd supports transparent restarts of itself that can be initiated by the `systemctl deamon-reexec` command. In order to do this, all Units must be able to serialise their state. We have to extend the serialisation and deserialisation code to include our modifications of the Service unit's data structure defined in the `src/core/service.h` file.

The most noteworthy change is the `restart_transparently` boolean flag within the Service unit. This flag enables the rest of our modifications.

## Configuration files

Our modifications added multiple directives. Parsing them is made straightforward with the already existing systemd's generic parsing facilities. Systemd uses a code generator to build its parser from a declarative definition located in the `src/core/load-fragment-gperf.gperf.m4` file. We have added a few lines with new directives, reusing existing methods for value parsing.

## File descriptor storage

The Service unit allows services to store their file descriptors across restarts within an internal data structure called `FDStore`. These file descriptors are then provided to the service using the environment variable protocol described in section \ref{the-file-descriptor-passing-protocol}. Our systemd modifications can use `FDStore` to do most of the work. When we store the service's file descriptors there, everything else will happen automatically.

However, there is a slight issue that we have to distinguish between libzedr provided file descriptors and systemd file descriptors. Libzedr file descriptors should be stored only during the restart itself, while the rest should be stored indefinitely. Identifying the origin of the file descriptors during cleanup turned out to be complicated, so in the end we duplicated almost the same functionality in our custom data structure being inspired by `FDStore`. We do not interact with `FDStore` in any way, so its functionality remains unchanged.

The built-in method to load the file descriptors into `FDStore` is by using the libsystemd library and `sd_notify_with_fds()` function \cite{SdNotify}, which sends the file descriptors over the systemd control Unix socket. We need to have the same functionality for standalone restarts and for other service managers. Moreover, the `sd_notify_with_fds()` function does not support sending multiple file descriptors with multiple identifiers atomically. That would make lifting the single communication channel limitation in the future more complicated than necessary. Therefore, we use our own Unix socket with our own protocol.

## Modifications of the restart procedure

Our primary focus is the restart job on the Service unit. However, the restart job is a stop job followed by a start job. Therefore, we modified the stop job handler, the `service_stop()` function. The first thing it does is a check whether we should perform the transparent restart. If that is true, the unit tries to transition into the newly added `SERVICE_TRANSPARENT_RESTART_PENDING` state (TRP state) by calling the `service_enter_transparent_restart_pending()` method. In all other cases, the standard stop procedure follows.

We added this state because two events have to occur. Systemd has to signal the service to restart and then wait for the service to send over its file descriptors. As discussed in Section \ref{systemds-data-model}, the automaton states usually refer to situations where systemd has to wait for something. So the addition of the new state matches the model. When in the TRP state, we are waiting for the file descriptors from the service.

The transition\footnotemark into the TRP state calls the libzedr restart server initialisation function `zedr_sm_server_init()`. This function returns a file descriptor, and we wait for an activity on the file descriptor, for a timeout or the `SIGCHLD` signal.

\footnotetext{i.e. the function \Verb"service\_enter\_transparent\_restart\_pending()" }

When everything goes well, I/O activity on the file descriptor will cause the libzedr's `zedr_sm_server_process()` function to be called and the activity processed. Then, we enter the `SERVICE_DEAD` state.

There are, however, several points where an error can occur. Any of the library calls can fail. When the `zedr_sm_server_init()` function fails, we do not enter the TRP state, but we return to `SERVICE_RUNNING` state reporting an error. The service is left running untouched. When an error occurs anywhere else, be it the other libzedr function or a timeout, the state of the service itself is unknown. It can be corrupted. It is probably trying to do a graceful shutdown. In such error cases we forcefully kill the service, bringing it to a state with known properties (i.e. it is not running). Then we enter the `SERVICE_DEAD` state.

Transitions from the TRP state always end in the `SERVICE_DEAD` state. That state is picked up by the ongoing restart job, and the standard start procedure continues. The `SERVICE_DEAD` state means that systemd stops monitoring the processes.

When the new instance starts, the old, still potentially running instance would be in the same cgroup as the new instance. Therefore, systemd will start monitoring the processes again, the old and the new instance of the service would share the same cgroup and the same Service unit. When a stop job for the Service appears, every process in the cgroup will get killed, including the old instance.

# User documentation of systemd

Documentation about using systemd with libzedr and transparent restarts enabled is located within libzedr's documentation. See Section \ref{documentation-of-libzedr}.
